// Create a new group
POST http://localhost:3001/groups HTTP/1.1
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjcsImlhdCI6MTY0ODQ2OTA1MzQ4NywiZXhwIjoxNjQ4NTU1NDUzNDg3fQ.TqF8RIYfnVxfgfgDTqzzUPKtail3BPGoEWvziGUdq4k

{
  "name": "Magic the Gathering group",
  "description": "games every weekend",
  "isPrivate": true
}
