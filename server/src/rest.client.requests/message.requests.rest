//Get all messages you have exchanged with a user
GET http://localhost:3001/messages/user/4 HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsImlhdCI6MTY0ODU1NDYzNzAyMSwiZXhwIjoxNjQ4NjQxMDM3MDIxfQ.w-qdUjr6i-YYAN3FJ7HnUSv_uOMR6iSdGfv93udFkTU

###
//GEt all session messages
GET http://localhost:3001/messages/session/4 HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsImlhdCI6MTY0ODIxNjkxMjg1MiwiZXhwIjoxNjQ4MzAzMzEyODUyfQ.EXks8QLNJc-NWIqwOztd6YqCcDQrGa6Y90XUf4BdjBk

###
// Post a message to a session message board
POST http://localhost:3001/messages/session/2 HTTP/1.1
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsImlhdCI6MTY0ODIxNjkxMjg1MiwiZXhwIjoxNjQ4MzAzMzEyODUyfQ.EXks8QLNJc-NWIqwOztd6YqCcDQrGa6Y90XUf4BdjBk


{
  "message": "Hello, nice weather"
}

###
//Send a message to another user

POST http://localhost:3001/messages/user/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsImlhdCI6MTY0ODIxNjkxMjg1MiwiZXhwIjoxNjQ4MzAzMzEyODUyfQ.EXks8QLNJc-NWIqwOztd6YqCcDQrGa6Y90XUf4BdjBk

{
  "message": "Hejsan"
}
