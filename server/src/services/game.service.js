import fetch from 'node-fetch';
import { API_EXPANSION, API_ID, API_SEARCH } from '../../constants.js';
import {
  filterOutExpansions,
  jsonToGame,
  result,
  sortGames,
  xmlToJson,
} from '../lib/utils.js';
import Game from '../models/game.model.js';
import UserGame from '../models/user.game.model.js';
import User from '../models/user.model.js';

export const getAll = async () => {
  try {
    const games = await Game.findAll();
    return result(games, 200);
  } catch (error) {
    throw Error('Error getting all games');
  }
};

export const addGame = async (game) => {
  try {
    const addedGame = await Game.create(game);
    return result(addedGame, 200);
  } catch (error) {
    throw Error('Error adding game');
  }
};

export const getById = async (id) => {
  try {
    const game = await Game.findByPk(id);

    if (game) return result(game, 200);

    const bggResult = await fetch(`${API_ID}${id}`);
    const jsonResult = await xmlToJson(await bggResult.text());
    const finalResult = jsonResult.items?.item?.[0];

    if (!finalResult) return result('No game found', 404);

    const jsonGame = jsonToGame(finalResult);

    await addGame(jsonGame);

    return result(jsonGame, 200);
  } catch (error) {
    throw Error('Error getting game by id');
  }
};

export const getUsersGames = async (userId, owned) => {
  try {
    const user = await User.findByPk(userId);

    if (!user) return result('No such user found', 400);

    const games = await UserGame.findAll({
      where: {
        userId,
        owned,
      },
    });

    if (games.length === 0) return result('No games found', 204);

    const gamePromises = games.map((game) => Game.findByPk(game.gameId));
    const fullGames = await Promise.all(gamePromises);

    return result(fullGames, 200);
  } catch (error) {
    throw Error('Error getting users games');
  }
};

export const getUsersGame = async (userId, gameId) => {
  try {
    const usersGame = await UserGame.findOne({
      where: {
        userId,
        gameId,
      },
    });
    if (!usersGame) return result('No game', 204);
    return result(usersGame, 200);
  } catch (error) {
    throw Error('Error getting a users game');
  }
};

export const search = async (queryString, limit = 15) => {
  try {
    const fetchedGames = await fetch(`${API_SEARCH}${queryString}`);
    const jsonGames = await xmlToJson(await fetchedGames.text());
    const games = jsonGames.items?.item;

    if (!games) return result('No games found', 404);

    const fetchedExpansions = await fetch(`${API_EXPANSION}${queryString}`);
    const jsonExpansions = await xmlToJson(await fetchedExpansions.text());
    const expansions = jsonExpansions.items?.item;
    const filtered = filterOutExpansions(games, expansions);

    const sortedGames = sortGames(filtered, queryString).slice(0, limit);

    const fullGamePromises = sortedGames.map((game) => getById(game.id));
    const fullGames = await Promise.all(fullGamePromises);

    return result(
      fullGames.map((game) => game.response),
      200,
    );
  } catch (error) {
    throw Error('Error searching game');
  }
};

export const editOwnedStatus = async (userId, gameId, owned) => {
  try {
    await UserGame.update(
      {
        owned,
      },
      {
        where: {
          userId,
          gameId,
        },
      },
    );
    return result('Game updated', 204);
  } catch (error) {
    throw Error('Error editing owned status');
  }
};

export const addGameToUser = async (userId, gameId, owned) => {
  try {
    const addedGame = await UserGame.create({ userId, gameId, owned });
    return result(addedGame, 200);
  } catch (error) {
    throw Error('Error adding game to user');
  }
};

export const deleteGameFromUser = async (gameId, userId) => {
  try {
    const deletedRows = await UserGame.destroy({ where: { userId, gameId } });
    if (!deletedRows) {
      return result('No such game on your games list', 404);
    }
    return result(deletedRows, 200);
  } catch (error) {
    throw Error('Error deleting a game from user');
  }
};
