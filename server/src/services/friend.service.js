import { Op } from 'sequelize';
import { result } from '../lib/utils.js';
import Friend from '../models/friend.model.js';
import User from '../models/user.model.js';

export const getSenderAndContact = (friends, userId) => {
  const promises = friends.map(async (friend) => {
    if (friend.receiverId === userId) {
      const user = await User.findByPk(friend.senderId);
      return user;
    }
    if (friend.senderId === userId) {
      const user = await User.findByPk(friend.receiverId);
      return user;
    }
    return [];
  });
  return Promise.all(promises);
};

export const allFriends = async (id, userId) => {
  try {
    if (id !== userId) {
      const friend = await Friend.findOne({
        where: {
          [Op.or]: [
            { [Op.and]: [{ senderId: id }, { receiverId: userId }] },
            { [Op.and]: [{ senderId: userId }, { receiverId: id }] },
          ],
        },
      });
      if (!friend) return result('Friend not found', 401);
      if (friend.status !== 'accepted') return result('Unauthorized', 401);
      const friendsList = await Friend.findAll({
        where: {
          [Op.or]: [
            { [Op.and]: [{ senderId: id }, { status: 'accepted' }] },
            { [Op.and]: [{ receiverId: id }, { status: 'accepted' }] },
          ],
        },
      });
      const friendsOfAFriend = await getSenderAndContact(friendsList, id);
      return result(friendsOfAFriend, 200);
    }
    const friends = await Friend.findAll({
      where: {
        [Op.or]: [
          { [Op.and]: [{ senderId: id }, { status: 'accepted' }] },
          { [Op.and]: [{ receiverId: id }, { status: 'accepted' }] },
        ],
      },
    });
    const usersFriends = await getSenderAndContact(friends, userId);
    return result(usersFriends, 200);
  } catch (error) {
    throw Error('Error getting your friends');
  }
};

export const add = async (senderId, receiverId) => {
  try {
    if (senderId === receiverId) {
      return result('You cannot send a friend request to yourself', 401);
    }
    const alreadySentRequest = await Friend.findOne({
      where: { senderId, receiverId },
    });
    if (alreadySentRequest !== null) {
      return result('You have already sent a friend request to this user', 401);
    }

    const foundFriendRequest = await Friend.findOne({
      where: { senderId: receiverId, receiverId: senderId },
    });

    if (foundFriendRequest !== null) {
      return result('You already have a pending friend request', 401);
    }

    const friend = { senderId, receiverId };

    const newFriend = await Friend.create({ ...friend });
    return result(newFriend, 200);
  } catch (error) {
    throw Error('Error trying to add user');
  }
};

export const verify = async (senderId, receiverId, status) => {
  try {
    const friend = await Friend.findOne({ where: { senderId, receiverId } });
    friend.set({ status });
    friend.save();
    return result(friend, 200);
  } catch (error) {
    throw Error('Error trying to verify user');
  }
};

export const checkFriendStatus = async (friendId, userId, id) => {
  try {
    if (userId !== id) {
      return result('Unauthorized', 401);
    }

    if (userId === friendId) {
      return result('You cannot have a friend request from yourself', 401);
    }

    const foundUser = await User.findByPk(friendId);

    if (foundUser === null) {
      return result('Not found', 404);
    }

    const friendStatus = await Friend.findOne({
      where: {
        senderId: {
          [Op.or]: [friendId, userId],
        },
        receiverId: {
          [Op.or]: [friendId, userId],
        },
      },
    });

    return friendStatus
      ? result(friendStatus.status, 200)
      : result('Not friends', 204);
  } catch (error) {
    throw Error('Error trying to check friend request status');
  }
};

export const remove = async (senderId, receiverId) => {
  try {
    const friend = await Friend.destroy({
      where: {
        [Op.or]: [
          { [Op.and]: [{ senderId }, { receiverId }] },
          { [Op.and]: [{ senderId: receiverId }, { receiverId: senderId }] },
        ],
      },
    });
    return result(friend, 200);
  } catch (error) {
    throw Error('Error trying to remove user from friends');
  }
};
