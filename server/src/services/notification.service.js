import { Op } from 'sequelize';
import sequelize from '../db/db.js';
import { result } from '../lib/utils.js';
import Friend from '../models/friend.model.js';
import Message from '../models/message.model.js';
import SessionMessage from '../models/session.message.model.js';

const getAll = async (userId) => {
  try {
    const friendRequests = await Friend.findAll({
      where: { [Op.and]: [{ receiverId: userId }, { status: 'pending' }] },
    });

    // eslint-disable-next-line no-unused-vars
    const [sessionRequests, metadata] = await sequelize.query(
      `SELECT user_id, session_id, status FROM users_sessions
      INNER JOIN sessions
      ON session_id = sessions.id
      WHERE creator_id=${userId} AND status='pending'`,
    );

    const unreadMessages = await Message.findAll({
      where: { receiverId: userId, hasRead: false },
    });

    const sessionMessages = await SessionMessage.findAll({ where: { senderId: userId } });

    const returnPackage = { friendRequests, sessionRequests, unreadMessages, sessionMessages };

    return result(returnPackage, 200);
  } catch (error) {
    throw Error('Error returning your notifications');
  }
};

export default getAll;
