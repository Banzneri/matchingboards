import { Op } from 'sequelize';
import { getDistanceBetweenUsers, result } from '../lib/utils.js';
import Session from '../models/session.model.js';
import UserSession from '../models/user.session.model.js';
import User from '../models/user.model.js';
import Game from '../models/game.model.js';
import { allFriends } from './friend.service.js';

const mapSession = async (session) => {
  const game = await Game.findByPk(session.gameId);
  const host = await User.findByPk(session.creatorId);
  const { id, date, time, description } = session;
  const mappedSession = { id, date, time, description, game, host };
  return mappedSession;
};

const mapSessions = async (sessions) => {
  const mappedSessions = await Promise.all(
    sessions.map(async (session) => {
      const mappedSession = await mapSession(session);
      return mappedSession;
    }),
  );
  return mappedSessions;
};

export const getAll = async () => {
  try {
    const sessions = await Session.findAll();
    const mappedSessions = await mapSessions(sessions);
    return result(mappedSessions, 200);
  } catch (error) {
    throw Error('Error getting all sessions');
  }
};

export const getById = async (id) => {
  try {
    const session = await Session.findByPk(id);
    const mappedSession = await mapSession(session);
    return result(mappedSession, 200);
  } catch (error) {
    throw Error('Error getting a session');
  }
};

export const add = async (session) => {
  try {
    const newSession = await Session.create({ ...session });
    const sessionId = newSession.dataValues.id;
    const status = 'accepted';
    const newUserSession = await UserSession.create({
      userId: session.creatorId,
      sessionId,
      status,
    });
    const returnPackage = { newSession, newUserSession };
    return result(returnPackage, 200);
  } catch (error) {
    throw Error('Error adding a session');
  }
};

export const getSessionParticipants = async (sessionId) => {
  try {
    const session = await Session.findByPk(sessionId);

    if (!session) return result('No session found', 404);

    const participants = await UserSession.findAll({
      where: {
        sessionId,
      },
    });

    if (!participants) return result('No users found', 500);

    if (participants.length === 1) return result('No joined users', 204);

    const filteredParticipants = participants.filter(
      (participant) => participant.userId !== session.creatorId,
    );

    const userPromises = filteredParticipants.map((filteredParticipant) =>
      User.findByPk(filteredParticipant.userId),
    );
    const users = await Promise.all(userPromises);
    return result(users, 200);
  } catch (error) {
    throw Error('Error getting session participants');
  }
};

export const verify = async (userId, creatorId, sessionId, status) => {
  try {
    const foundSessionCreator = await Session.findOne({
      where: { id: sessionId },
    });
    if (foundSessionCreator.creatorId !== creatorId) {
      return result(
        'Only the session creator can accept or reject invites',
        401,
      );
    }

    const userSession = await UserSession.findOne({
      where: { userId, sessionId },
    });
    if (!userSession) {
      return result('No session found!', 404);
    }

    userSession.set({ status });
    userSession.save();
    return result(userSession.status, 200);
  } catch (error) {
    throw Error('Error verifying session status');
  }
};

export const join = async (userId, sessionId) => {
  try {
    const joinedSession = await UserSession.findOne({
      where: { [Op.and]: [{ sessionId }, { userId }] },
    });

    if (joinedSession) return result('Session already joined', 409);

    const sessionParticipants = await getSessionParticipants(sessionId);
    const userSession = await Session.findByPk(sessionId);
    const sessionGame = await Game.findByPk(userSession.gameId);
    const participantCount =
      sessionParticipants.code === 204
        ? 1
        : sessionParticipants.response.length + 1;

    if (participantCount >= sessionGame.maxPlayers) {
      return result('Unable to participate session already full!', 409);
    }

    const session = { userId, sessionId };

    const user = await UserSession.create({ ...session });
    return result(user, 200);
  } catch (error) {
    throw Error('Error joining session');
  }
};

export const removeUserFromSession = async (
  sessionId,
  userToRemoveId,
  currentUserId,
) => {
  try {
    const session = await Session.findByPk(sessionId);

    if (!session) return result('No session found', 404);

    if (session.creatorId === userToRemoveId) {
      return result("You're the host, you can't leave!", 401);
    }

    if (
      !(currentUserId === session.creatorId || userToRemoveId === currentUserId)
    ) {
      return result(
        'You are not authorized to remove this user from session',
        401,
      );
    }

    const userSession = await UserSession.findOne({
      where: { sessionId, userId: userToRemoveId },
    });

    if (!userSession) return result('No such user in session', 404);

    await UserSession.destroy({
      where: {
        sessionId,
        userId: userToRemoveId,
      },
    });
    return result('User removed from session', 200);
  } catch (error) {
    throw Error('Error removing user from session');
  }
};

export const deleteByID = async (id, userId) => {
  try {
    const foundSession = await Session.findOne({ where: { id } });
    if (!foundSession) {
      return result('Session not found', 404);
    }
    if (foundSession.creatorId !== userId) {
      return result('Only the session creator can delete the session', 403);
    }
    await Session.destroy({
      where: {
        id,
      },
    });
    return result('Session deleted', 200);
  } catch (error) {
    throw Error('Error deleting a session');
  }
};

export const getUsersJoinedSessions = async (userId) => {
  const sessionsResponse = await UserSession.findAll({ where: { userId } });

  if (!sessionsResponse) return result('No sessions found', 404);

  const sessionPromises = sessionsResponse.map((session) =>
    Session.findByPk(session.sessionId),
  );

  const sessions = await Promise.all(sessionPromises);
  const mappedSessions = await mapSessions(sessions);

  return result(mappedSessions, 200);
};

export const getNearbySessions = async (
  user,
  gameId,
  onlyFriends,
  distance,
) => {
  try {
    const allSessions = await Session.findAll(
      gameId
        ? { where: { gameId, date: { [Op.gte]: Date.now() } } }
        : { where: { date: { [Op.gte]: Date.now() } } },
    );

    const filterSelfSessions = allSessions.filter(
      (session) => session.creatorId !== user.id,
    );

    const hostPromises = filterSelfSessions.map((session) =>
      User.findByPk(session.creatorId),
    );
    const hosts = await Promise.all(hostPromises);
    const friends = (await allFriends(user.id, user.id)).response;

    const nearbySessions = filterSelfSessions.filter((session) => {
      const host = hosts.find((creator) => creator.id === session.creatorId);
      if (onlyFriends === 'true') {
        if (!friends.find((friend) => friend.id === host.id)) {
          return false;
        }
      }
      return getDistanceBetweenUsers(user, session) < distance;
    });

    const mappedSessions = await mapSessions(nearbySessions);
    return result(mappedSessions, 200);
  } catch (error) {
    throw Error('Error getting sessions by distance');
  }
};
