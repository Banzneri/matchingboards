import { Op } from 'sequelize';
import Message from '../models/message.model.js';
import SessionMessage from '../models/session.message.model.js';
import UserSession from '../models/user.session.model.js';
import { result } from '../lib/utils.js';
import User from '../models/user.model.js';

export const messagesById = async (id, userId) => {
  try {
    if (id === userId) {
      return result("You can't send a message to yourself", 400);
    }
    const messages = await Message.findAll({
      where: {
        [Op.or]: [
          { [Op.and]: [{ senderId: id }, { receiverId: userId }] },
          { [Op.and]: [{ senderId: userId }, { receiverId: id }] },
        ],
      },
      order: [['sentAt', 'ASC']],
    });
    return result(messages, 200);
  } catch (error) {
    throw Error('Error getting messages');
  }
};

export const allSessionMessages = async (sessionId, userId) => {
  try {
    const foundUserSession = await UserSession.findOne({
      where: { userId, sessionId },
    });
    if (foundUserSession === null) {
      return result(
        'You need to join the session to read the messageboard',
        403,
      );
    }

    const messages = await SessionMessage.findAll({
      where: { sessionId },
      order: [['sentAt', 'DESC']],
    });
    const senderPromises = messages.map((message) =>
      User.findByPk(message.senderId),
    );
    const senders = await Promise.all(senderPromises);
    const mappedMessages = messages.map((msg) => {
      const senderUser = senders.find((sender) => msg.senderId === sender.id);
      const { id, message, sentAt } = msg;
      return { id, message, senderUser, sentAt };
    });
    return result(mappedMessages, 200);
  } catch (error) {
    throw Error('Error getting session messages');
  }
};

export const newSessionMessage = async (sessionId, senderId, message) => {
  try {
    const foundUserSession = await UserSession.findOne({
      where: { userId: senderId, sessionId },
    });
    if (foundUserSession === null) {
      return result(
        'You need to join the session to post messages on the messageboard',
        403,
      );
    }

    const sessionsMessage = { sessionId, message, senderId };
    const newMessage = await SessionMessage.create({ ...sessionsMessage });
    return result(newMessage, 200);
  } catch (error) {
    throw Error('Error posting a new message to session');
  }
};

export const newUserMessage = async (senderId, receiverId, message) => {
  try {
    if (senderId === receiverId) {
      return result('You cannot send a message to yourself', 403);
    }
    const newMessage = { senderId, receiverId, message };
    const userMessage = await Message.create({ ...newMessage });
    return result(userMessage, 200);
  } catch (error) {
    throw Error('Error posting a new message to user');
  }
};
