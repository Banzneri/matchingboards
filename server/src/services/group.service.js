import Group from '../models/group.model.js';
import UserGroup from '../models/user.group.model.js';
import { result } from '../lib/utils.js';

export const create = async (name, description, creatorId, isprivate) => {
  try {
    const newGroup = {
      name,
      description,
      creatorId,
      isprivate,
    };
    const createdGroup = await Group.create({ ...newGroup });
    return result(createdGroup, 200);
  } catch (error) {
    throw Error('Error trying to create a group');
  }
};

export const invite = async (userId, groupId) => {
  try {
    const member = {
      userId,
      groupId,
    };
    const user = await UserGroup.create({ ...member });
    return result(user, 200);
  } catch (error) {
    throw Error('Error trying to send invite to a user');
  }
};
