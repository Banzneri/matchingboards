import Rating from '../models/rating.model.js';
import { result } from '../lib/utils.js';

const addRating = async (giverId, receivingId, value) => {
  try {
    if (giverId === receivingId) {
      return result('You cannot give a rating to yourself', 403);
    }
    const newRating = { giverId, receivingId, value };
    const userRating = await Rating.create({ ...newRating });
    return result(userRating, 200);
  } catch (error) {
    throw Error('Error adding a user rating');
  }
};

export default addRating;
