import { Op } from 'sequelize';
import User from '../models/user.model.js';
import {
  genJwt,
  genSaltHash,
  result,
  validPassword,
  isAdmin,
} from '../lib/utils.js';

export const getAll = async () => {
  try {
    const users = await User.findAll();
    return result(users, 200);
  } catch (error) {
    throw Error('Error getting all users');
  }
};

export const getById = async (id) => {
  try {
    const user = await User.findByPk(id);
    return result(user, 200);
  } catch (error) {
    throw Error('Error getting user by id');
  }
};

export const getByEmail = async (user) => {
  const userToAdd = await User.findOne({
    where: { email: user.email },
  });
  return result(userToAdd, 200);
};

const sortResults = (filteredUsers, userName) => {
  const username = userName.toLowerCase();
  const search = new RegExp(username, 'g');

  const exactString = filteredUsers.filter(
    (user) => user.dataValues.userName.toLowerCase() === username,
  );

  exactString.sort((a, b) => a - b);

  const startsWith = filteredUsers.filter(
    (user) => user.dataValues.userName.toLowerCase().substring(0, username.length) === username &&
      user.dataValues.userName.toLowerCase() !== username,
  );

  startsWith.sort((a, b) => a - b);

  const containsString = filteredUsers.filter(
    (user) => user.dataValues.userName.toLowerCase().match(search) &&
      user.dataValues.userName.toLowerCase() !== username &&
      user.dataValues.userName.toLowerCase().substring(0, username.length) !== username,
  );

  containsString.sort((a, b) => a - b);
  return exactString.concat(startsWith, containsString);
};

export const add = async (user) => {
  try {
    const newUser = await User.create({ ...user });
    return result(newUser, 200);
  } catch (error) {
    throw Error('Error adding user');
  }
};

export const search = async (userId, userName) => {
  try {
    const users = await User.findAll({
      where: {
        userName: {
          [Op.iLike]: `%${userName}%`,
        },
      },
    });
    const filteredUsers = users.filter(
      (user) => user.id !== userId && user.id !== 0,
    );
    const results = sortResults(filteredUsers, userName);
    return result(results, 200);
  } catch (error) {
    throw Error('Error trying to search for a user');
  }
};

export const deleteById = async (id, user) => {
  try {
    if (id !== user.id) {
      const adminStatus = isAdmin(user.role);
      if (!adminStatus) {
        return result('You are unauthorized', 401);
      }
    }

    await User.destroy({
      where: {
        id,
      },
    });
    return result('User deleted', 200);
  } catch (error) {
    throw Error('Error deleting user by id');
  }
};

export const update = async (user, id) => {
  try {
    await User.update(
      {
        ...user,
      },
      {
        where: {
          id,
        },
      },
    );

    return result('User info updated successfully', 200);
  } catch (error) {
    throw Error('Error updating information');
  }
};

export const register = async (userName, email, password, zipCode) => {
  try {
    const user = await User.findOne({
      where: {
        email,
      },
    });

    if (user) return result('Email already in use', 409);

    const { salt, hash } = genSaltHash(password);

    const newUser = {
      email,
      userName,
      zipCode,
      salt,
      hash,
    };

    await add(newUser);
    return result(newUser, 200);
  } catch (error) {
    throw Error('Error registering user');
  }
};

export const login = async (email, password) => {
  const errorMessage = 'Wrong username or password';

  try {
    const user = await User.findOne({
      where: {
        email,
      },
    });

    if (!user) return result(errorMessage, 404);

    const { hash, salt } = user;

    if (validPassword(password, hash, salt)) {
      const { token } = genJwt(user);
      return result({ user, token }, 200);
    }

    return result(errorMessage, 400);
  } catch (error) {
    throw Error('Error logging in');
  }
};

export const authenticate = async (user) => result({ user }, 200);

export const changePassword = async (
  userId,
  oldPassword,
  newPassword,
  confirmNewPassword,
) => {
  try {
    const user = await User.findByPk(userId);

    if (!user) return result('User not found', 404);

    if (
      validPassword(oldPassword, user.hash, user.salt) &&
      newPassword === confirmNewPassword
    ) {
      const { hash, salt } = genSaltHash(newPassword);
      user.set({ hash, salt });
      user.save();
      return result('Password successfully updated', 200);
    }

    return result('Invalid password', 400);
  } catch (error) {
    throw Error('Error changing password');
  }
};
