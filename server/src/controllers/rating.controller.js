import { body, param } from 'express-validator';
import addRating from '../services/rating.service.js';
import { handleResponse } from '../lib/utils.js';

export const addUserRating = async (req, res) => {
  const giverId = req.user.id;
  const receivingId = Number(req.params.id);
  const value = Number(req.body.value);

  await handleResponse(req, res, addRating, [
    giverId,
    receivingId,
    value,
  ]);
};

export const validate = (controller) => {
  const isValidReceivingId = () => param('id', 'Id must be an integer').isInt();
  const isValidRating = () => body('value', 'Value must be an integer').isInt({ min: 1, max: 5 });

  switch (controller) {
    case 'addUserRating':
      return [isValidReceivingId(), isValidRating()];
    default:
      return [];
  }
};
