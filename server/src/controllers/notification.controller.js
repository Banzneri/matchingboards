import { handleResponse } from '../lib/utils.js';
import getAll from '../services/notification.service.js';

const getNewNotifications = async (req, res) => {
  const userId = req.user.id;
  await handleResponse(req, res, getAll, [userId]);
};

export default getNewNotifications;
