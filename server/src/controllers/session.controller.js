import { body, param, query } from 'express-validator';
import { handleResponse } from '../lib/utils.js';
import * as Session from '../services/session.service.js';

export const getAllSessions = async (req, res) => {
  await handleResponse(req, res, Session.getAll, []);
};

export const getSessionById = async (req, res) => {
  const id = Number(req.params.id);
  await handleResponse(req, res, Session.getById, [id]);
};

export const addSession = async (req, res) => {
  const creatorId = req.user.id;
  const session = req.body;
  session.creatorId = creatorId;
  await handleResponse(req, res, Session.add, [session]);
};

export const getSessionParticipants = async (req, res) => {
  const { id } = req.params;
  await handleResponse(req, res, Session.getSessionParticipants, [id]);
};

export const verifySession = async (req, res) => {
  const creatorId = req.user.id;
  const id = Number(req.params.id);
  const { status, userId } = req.body;
  await handleResponse(req, res, Session.verify, [
    userId,
    creatorId,
    id,
    status,
  ]);
};

export const joinSession = async (req, res) => {
  const { id } = req.user;
  const { sessionId } = req.body;
  await handleResponse(req, res, Session.join, [id, sessionId]);
};

export const removeUserFromSession = async (req, res) => {
  const { userId, sessionId } = req.body;
  const { id } = req.user;
  await handleResponse(req, res, Session.removeUserFromSession, [
    sessionId,
    userId,
    id,
  ]);
};

export const deleteSessionById = async (req, res) => {
  const userId = req.user.id;
  const id = Number(req.params.id);
  await handleResponse(req, res, Session.deleteByID, [id, userId]);
};

export const getUsersSessions = async (req, res) => {
  const { id } = req.params;
  await handleResponse(req, res, Session.getUsersJoinedSessions, [id]);
};

export const getNearbySessions = async (req, res) => {
  const { user } = req;
  const { gameId, distance, onlyFriends } = req.query;
  await handleResponse(req, res, Session.getNearbySessions, [
    user,
    gameId,
    onlyFriends,
    distance,
  ]);
};

export const validate = (method) => {
  const validId = () => param('id', 'id must be an integer').isInt();
  const validDate = () => body('date', 'invalid date').isDate();
  const validGameId = () => body('gameId', 'invalid game id').isInt();
  const validLon = () =>
    body('longitude', 'invalid longitude').isDecimal({
      min: -180,
      max: 180,
    });
  const validLat = () =>
    body('latitude', 'invalid latitude').isDecimal({
      min: -90,
      max: 90,
    });
  const validDescription = () =>
    body('description', 'invalid description').isString();
  const validSessionStatus = () =>
    body('status', 'invalid status').isIn(['rejected', 'accepted']);
  const validBodyId = (idName) => body(idName, `invalid ${idName}`).isInt();
  const optionalValidGameId = () =>
    query('gameId', 'game id must be an integer').isInt().optional();
  const validDistance = () =>
    query('distance', 'distance must be an integer').isInt();
  const validOnlyFriends = () =>
    query('onlyFriends', 'onlyFriends must be a boolean').isBoolean();

  switch (method) {
    case 'getSessionById':
      return [validId()];
    case 'addSession':
      return [
        validDate(),
        validGameId(),
        validLon(),
        validLat(),
        validDescription(),
      ];
    case 'verifySession':
      return [validId(), validBodyId('userId'), validSessionStatus()];
    case 'deleteSessionById':
      return [validId()];
    case 'joinSession':
      return [validBodyId('sessionId')];
    case 'removeUserFromSession':
      return [validBodyId('userId'), validBodyId('sessionId')];
    case 'getNearbySessions':
      return [optionalValidGameId(), validDistance(), validOnlyFriends()];
    case 'getSessionParticipants':
      return [validId()];
    case 'getUsersSessions':
      return [validId()];
    default:
      return [];
  }
};
