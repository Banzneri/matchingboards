import { param, body, query } from 'express-validator';
import { handleResponse } from '../lib/utils.js';
import * as Friend from '../services/friend.service.js';

export const friendsOfAUser = async (req, res) => {
  const id = Number(req.params.id);
  const userId = req.user.id;
  await handleResponse(req, res, Friend.allFriends, [id, userId]);
};

export const addFriend = async (req, res) => {
  const userId = req.user.id;
  const id = Number(req.params.id);
  await handleResponse(req, res, Friend.add, [userId, id]);
};

export const checkFriend = async (req, res) => {
  const { id } = req.user;
  const { friendId, userId } = req.query;
  await handleResponse(req, res, Friend.checkFriendStatus, [
    friendId,
    Number(userId),
    Number(id),
  ]);
};

export const verifyFriend = async (req, res) => {
  const { id } = req.user;
  const { senderId, status } = req.body;
  await handleResponse(req, res, Friend.verify, [senderId, id, status]);
};

export const removeFriend = async (req, res) => {
  const userId = req.user.id;
  const id = Number(req.params.id);
  await handleResponse(req, res, Friend.remove, [userId, id]);
};

export const validate = (method) => {
  const validId = () => param('id', 'id must be an integer').isInt();

  const validFriendId = () =>
    query('friendId', 'friendId must be an integer').isInt();

  const validUserId = () =>
    query('userId', 'userId must be an integer').isInt();

  const validStatus = () =>
    body('status', 'not a valid status').isIn(['accepted', 'rejected']);

  const validSenderId = () =>
    body('senderId', 'senderId must be an integer').isInt();

  switch (method) {
    case 'friendsOfAUser':
      return [validId()];
    case 'checkFriend':
      return [validFriendId(), validUserId()];
    case 'removeFriend':
      return [validId()];
    case 'addFriend':
      return [validId()];
    case 'verifyFriend':
      return [validStatus(), validSenderId()];
    default:
      return [];
  }
};
