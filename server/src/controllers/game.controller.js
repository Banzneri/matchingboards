import { param, query } from 'express-validator';
import { handleResponse } from '../lib/utils.js';
import * as Game from '../services/game.service.js';

export const getAllGames = async (req, res) => {
  await handleResponse(req, res, Game.getAll, []);
};

export const getGameById = async (req, res) => {
  const { id } = req.params;
  await handleResponse(req, res, Game.getById, [id]);
};

export const getUsersGames = async (req, res) => {
  const { id } = req.params;
  const { owned } = req.query;
  await handleResponse(req, res, Game.getUsersGames, [id, owned]);
};

export const getUsersGame = async (req, res) => {
  const { id } = req.user;
  const { gameId } = req.params;
  await handleResponse(req, res, Game.getUsersGame, [id, gameId]);
};

export const searchGames = async (req, res) => {
  const { queryString, limit } = req.query;
  await handleResponse(req, res, Game.search, [queryString, limit]);
};

export const addGameToUser = async (req, res) => {
  const { id } = req.params;
  const { owned } = req.query;
  const userId = req.user.id;
  await handleResponse(req, res, Game.addGameToUser, [userId, id, owned]);
};

export const editOwnedStatus = async (req, res) => {
  const { id } = req.user;
  const { gameId } = req.params;
  const { owned } = req.query;
  await handleResponse(req, res, Game.editOwnedStatus, [
    id,
    Number(gameId),
    owned,
  ]);
};

export const deleteGameFromUser = async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  await handleResponse(req, res, Game.deleteGameFromUser, [id, userId]);
};

export const validate = (method) => {
  const validId = () => param('id', 'id must be an integer').isInt();
  const validOwned = () =>
    query('owned', 'the query "owned" should be a boolean').isBoolean();
  const validQueryString = () =>
    query('queryString', 'QueryString must be a string of minimum 1 length')
      .isString()
      .isLength({ min: 1 });
  const validLimit = () =>
    query('limit', 'limit must be an integer').optional().isInt();
  switch (method) {
    case 'getUsersGames':
      return [validId(), validOwned()];
    case 'getGameById':
      return [validId()];
    case 'addGameToUser':
      return [validId(), validOwned()];
    case 'deleteGameFromUser':
      return [validId()];
    case 'searchGames':
      return [validQueryString(), validLimit()];
    default:
      return [];
  }
};
