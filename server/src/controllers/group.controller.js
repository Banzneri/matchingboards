import { body } from 'express-validator';
import { handleResponse } from '../lib/utils.js';
import * as Group from '../services/group.service.js';

export const createGroup = async (req, res) => {
  const { name, description, isprivate } = req.body;
  const { id } = req.user;

  await handleResponse(req, res, Group.create, [
    name,
    description,
    id,
    isprivate,
  ]);
};

export const temp = () => {};

export const inviteUser = async (req, res) => {
  const { id } = req.user;
  const { groupId } = req.body;
  await handleResponse(req, res, Group.invite, [id, groupId]);
};

export const validate = (method) => {
  const validId = () => body('groupId', 'id must be an integer').isInt();
  switch (method) {
    case 'inviteUser':
      return [validId()];
    default:
      return [];
  }
};
