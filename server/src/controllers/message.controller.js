import { body, param } from 'express-validator';
import { handleResponse } from '../lib/utils.js';
import * as Message from '../services/message.service.js';

export const getUsersMessages = async (req, res) => {
  const id = Number(req.params.id);
  const userId = req.user.id;
  await handleResponse(req, res, Message.messagesById, [id, userId]);
};

export const getSessionMessages = async (req, res) => {
  const id = Number(req.params.id);
  const userId = req.user.id;
  await handleResponse(req, res, Message.allSessionMessages, [id, userId]);
};

export const postSessionMessage = async (req, res) => {
  const id = Number(req.params.id);
  const { message } = req.body;
  const userId = req.user.id;
  await handleResponse(req, res, Message.newSessionMessage, [
    id,
    userId,
    message,
  ]);
};

export const postUserMessage = async (req, res) => {
  const senderId = req.user.id;
  const { message } = req.body;
  const id = Number(req.params.id);
  await handleResponse(req, res, Message.newUserMessage, [
    senderId,
    id,
    message,
  ]);
};

export const validate = (method) => {
  const validId = () => param('id', 'id must be an integer').isInt();
  const validMessage = () =>
    body('message', 'message must be at least 1 character long string')
      .isString()
      .isLength({
        min: 1,
      });

  switch (method) {
    case 'getUsersMessages':
      return [validId()];
    case 'getSessionMessages':
      return [validId()];
    case 'postSessionMessage':
      return [validId(), validMessage()];
    case 'postUserMessage':
      return [validId(), validMessage()];
    default:
      return [];
  }
};
