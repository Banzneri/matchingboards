import { DataTypes } from 'sequelize';
import sequelize from '../db/db.js';
import User from './user.model.js';

const Rating = sequelize.define(
  'Rating',
  {
    value: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    tableName: 'ratings',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    underscored: true,
  },
);

User.belongsToMany(User, {
  as: 'giver_id',
  through: Rating,
  foreignKey: 'giverId',
  onUpdate: 'CASCADE',
  onDelete: 'SET DEFAULT',
  hooks: true,
});

User.belongsToMany(User, {
  as: 'receiving_id',
  through: Rating,
  foreignKey: 'receivingId',
  onUpdate: 'CASCADE',
  onDelete: 'SET DEFAULT',
  hooks: true,
});

export default Rating;
