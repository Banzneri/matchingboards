import { DataTypes } from 'sequelize';
import sequelize from '../db/db.js';
import Session from './session.model.js';
import User from './user.model.js';

const UserSession = sequelize.define(
  'UserSession',
  {
    status: {
      type: DataTypes.ENUM('pending', 'accepted', 'rejected'),
      defaultValue: 'accepted',
    },
  },
  {
    tableName: 'users_sessions',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    underscored: true,
  },
);

User.belongsToMany(Session, {
  through: UserSession,
  foreignKey: 'userId',
  onUpdate: 'CASCADE',
  onDelete: 'CASCADE',
  hooks: true,
});
Session.belongsToMany(User, {
  through: UserSession,
  foreignKey: 'sessionId',
  onUpdate: 'CASCADE',
  onDelete: 'CASCADE',
  hooks: true,
});

export default UserSession;
