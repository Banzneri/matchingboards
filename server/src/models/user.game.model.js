import { DataTypes } from 'sequelize';
import sequelize from '../db/db.js';
import Game from './game.model.js';
import User from './user.model.js';

const UserGame = sequelize.define(
  'UserGame',
  {
    owned: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    tableName: 'users_games',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    underscored: true,
  },
);

User.belongsToMany(Game, {
  as: 'user_id',
  through: UserGame,
  foreignKey: 'userId',
  onUpdate: 'CASCADE',
  onDelete: 'CASCADE',
  hooks: true,
});
Game.belongsToMany(User, {
  as: 'game_id',
  through: UserGame,
  foreignKey: 'gameId',
  onUpdate: 'CASCADE',
  onDelete: 'CASCADE',
  hooks: true,
});

export default UserGame;
