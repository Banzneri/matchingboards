import sequelize from '../db/db.js';
import Group from './group.model.js';
import User from './user.model.js';

const UserGroup = sequelize.define(
  'UserGroup',
  {
    tableName: 'users_groups',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    underscored: true,
  },
);

User.belongsToMany(
  Group,
  { through: UserGroup,
    foreignKey: 'UserId',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
    hooks: true,
  });

Group.belongsToMany(
  User,
  { through: UserGroup,
    foreignKey: 'groupId',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
    hooks: true,
  });
export default UserGroup;
