import { DataTypes } from 'sequelize';
import sequelize from '../db/db.js';

const SessionMessage = sequelize.define(
  'SessionMessage',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    senderId: {
      type: DataTypes.INTEGER,
      field: 'sender_id',
      allowNull: false,
      defaultValue: '0',
      references: {
        value: 'users',
        key: 'id',
      },
      onDelete: 'SET DEFAULT',
      onUpdate: 'CASCADE',
    },
    sessionId: {
      type: DataTypes.INTEGER,
      field: 'session_id',
      allowNull: false,
      refrences: {
        value: 'sessions',
        key: 'id',
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
    sentAt: {
      type: DataTypes.DATE,
      field: 'sent_at',
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  },
  {
    tableName: 'session_messages',
    createdAt: false,
    updatedAt: false,
  },
);

export default SessionMessage;
