import { DataTypes } from 'sequelize';
import sequelize from '../db/db.js';

const Message = sequelize.define(
  'Message',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    senderId: {
      type: DataTypes.INTEGER,
      field: 'sender_id',
      allowNull: false,
      defaultValue: '0',
      references: {
        value: 'users',
        key: 'id',
      },
      onDelete: 'SET DEFAULT',
      onUpdate: 'CASCADE',
    },
    receiverId: {
      type: DataTypes.INTEGER,
      field: 'receiver_id',
      allowNull: false,
      defaultValue: '0',
      references: {
        value: 'users',
        key: 'id',
      },
      onDelete: 'SET DEFAULT',
      onUpdate: 'CASCADE',
    },
    sentAt: {
      type: DataTypes.DATE,
      field: 'sent_at',
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    hasRead: {
      type: DataTypes.BOOLEAN,
      field: 'has_read',
      allowNull: false,
      defaultValue: false,
    },
  },
  {
    tableName: 'messages',
    createdAt: false,
    updatedAt: false,
  },
);

export default Message;
