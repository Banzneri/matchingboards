import { DataTypes } from 'sequelize';
import sequelize from '../db/db.js';

const User = sequelize.define(
  'User',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    userName: {
      type: DataTypes.STRING,
      field: 'username',
      allowNull: false,
      unique: true,
    },
    imageUrl: {
      type: DataTypes.STRING,
      field: 'image_url',
    },
    description: {
      type: DataTypes.STRING,
    },
    salt: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    hash: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    role: {
      type: DataTypes.ENUM('regular', 'admin', 'eventOrganizer'),
      defaultValue: 'regular',
    },
    zipCode: {
      type: DataTypes.INTEGER,
      field: 'zip_code',
    },
    latitude: {
      type: DataTypes.DECIMAL,
      defaultValue: '61.498001',
    },
    longitude: {
      type: DataTypes.DECIMAL,
      defaultValue: '23.760771',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
      defaultValue: null,
    },
  },
  {
    tableName: 'users',
  },
);

export default User;
