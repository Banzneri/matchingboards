import { DataTypes } from 'sequelize';
import sequelize from '../db/db.js';

const Blacklist = sequelize.define(
  'Blacklist',
  {
    userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
      primaryKey: true,
      allowNull: false,
      references: {
        value: 'users',
        key: 'id',
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
    blacklistedId: {
      type: DataTypes.INTEGER,
      field: 'blacklisted_id',
      primaryKey: true,
      allowNull: false,
      references: {
        value: 'users',
        key: 'id',
      },
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  },
  {
    tableName: 'blacklist',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
  },
);

export default Blacklist;
