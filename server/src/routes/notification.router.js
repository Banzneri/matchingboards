import passport from 'passport';
import { Router } from 'express';
import getNewNotifications from '../controllers/notification.controller.js';
import {
  jwtStrategy,
  passportJwt,
} from '../middleware/passport-jwt.middleware.js';

const notificationRouter = Router({ mergeParams: true });
passport.use(jwtStrategy);

notificationRouter.get('/', passportJwt(), getNewNotifications);

export default notificationRouter;
