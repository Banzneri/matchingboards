import passport from 'passport';
import { Router } from 'express';
import * as controller from '../controllers/group.controller.js';
import {
  jwtStrategy,
  passportJwt,
} from '../middleware/passport-jwt.middleware.js';

const groupRouter = Router({ mergeParams: true });
passport.use(jwtStrategy);

groupRouter.post('/', passportJwt(), controller.createGroup);
groupRouter.post('/invite', passportJwt(), controller.validate('inviteUser'), controller.inviteUser);

export default groupRouter;
