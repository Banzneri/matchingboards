import { Router } from 'express';
import passport from 'passport';
import * as controller from '../controllers/game.controller.js';
import {
  jwtStrategy,
  passportJwt,
} from '../middleware/passport-jwt.middleware.js';

const gameRouter = Router({ mergeParams: true });
passport.use(jwtStrategy);

gameRouter.get('/', controller.getAllGames);
gameRouter.get(
  '/:id',
  controller.validate('getGameById'),
  controller.getGameById,
);
gameRouter.get(
  '/search/name',
  controller.validate('searchGames'),
  controller.searchGames,
);
gameRouter.post(
  '/users/:id',
  passportJwt(),
  controller.validate('addGameToUser'),
  controller.addGameToUser,
);
gameRouter.put('/users/:gameId', passportJwt(), controller.editOwnedStatus);
gameRouter.delete(
  '/users/:id',
  passportJwt(),
  controller.validate('deleteGameFromUser'),
  controller.deleteGameFromUser,
);
gameRouter.get(
  '/users/:id',
  controller.validate('getUsersGames'),
  controller.getUsersGames,
);
gameRouter.get('/users/game/:gameId', passportJwt(), controller.getUsersGame);

export default gameRouter;
