import passport from 'passport';
import { Router } from 'express';
import * as controller from '../controllers/message.controller.js';
import {
  jwtStrategy,
  passportJwt,
} from '../middleware/passport-jwt.middleware.js';

const messageRouter = Router({ mergeParams: true });
passport.use(jwtStrategy);

messageRouter.get(
  '/user/:id',
  passportJwt(),
  controller.validate('getUsersMessages'),
  controller.getUsersMessages,
);
messageRouter.get(
  '/session/:id',
  passportJwt(),
  controller.validate('getSessionMessages'),
  controller.getSessionMessages,
);
messageRouter.post(
  '/session/:id',
  passportJwt(),
  controller.validate('postSessionMessage'),
  controller.postSessionMessage,
);

messageRouter.post('/user/:id',
  passportJwt(),
  controller.validate('postUserMessage'),
  controller.postUserMessage,
);

export default messageRouter;
