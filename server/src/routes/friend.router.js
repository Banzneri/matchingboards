import passport from 'passport';
import { Router } from 'express';
import * as controller from '../controllers/friend.controller.js';
import {
  jwtStrategy,
  passportJwt,
} from '../middleware/passport-jwt.middleware.js';

const friendRouter = Router({ mergeParams: true });
passport.use(jwtStrategy);

friendRouter.post(
  '/add/:id',
  passportJwt(),
  controller.validate('addFriend'),
  controller.addFriend,
);
friendRouter.patch(
  '/verify',
  passportJwt(),
  controller.validate('verifyFriend'),
  controller.verifyFriend,
);
friendRouter.get(
  '/id/:id',
  passportJwt(),
  controller.validate('friendsOfAUser'),
  controller.friendsOfAUser,
);
friendRouter.get(
  '/checkFriendStatus',
  passportJwt(),
  controller.validate('checkFriend'),
  controller.checkFriend,
);
friendRouter.delete(
  '/remove/:id',
  passportJwt(),
  controller.validate('removeFriend'),
  controller.removeFriend,
);

export default friendRouter;
