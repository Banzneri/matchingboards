import passport from 'passport';
import { Router } from 'express';
import * as controller from '../controllers/session.controller.js';
import {
  jwtStrategy,
  passportJwt,
} from '../middleware/passport-jwt.middleware.js';

const sessionRouter = Router({ mergeParams: true });
passport.use(jwtStrategy);

sessionRouter.get(
  '/user/:id',
  passportJwt(),
  controller.validate('getUsersSessions'),
  controller.getUsersSessions,
);
sessionRouter.get(
  '/nearby',
  passportJwt(),
  controller.validate('getNearbySessions'),
  controller.getNearbySessions,
);
sessionRouter.get('/', controller.getAllSessions);
sessionRouter.post(
  '/join',
  passportJwt(),
  controller.validate('joinSession'),
  controller.joinSession,
);
sessionRouter.delete(
  '/users',
  passportJwt(),
  controller.validate('removeUserFromSession'),
  controller.removeUserFromSession,
);
sessionRouter.post(
  '/',
  passportJwt(),
  controller.validate('addSession'),
  controller.addSession,
);
sessionRouter.get(
  '/participants/:id',
  passportJwt(),
  controller.validate('getSessionParticipants'),
  controller.getSessionParticipants,
);
sessionRouter.get(
  '/:id',
  controller.validate('getSessionById'),
  controller.getSessionById,
);

sessionRouter.patch(
  '/verify/:id',
  passportJwt(),
  controller.validate('verifySession'),
  controller.verifySession,
);

sessionRouter.delete(
  '/:id',
  passportJwt(),
  controller.validate('deleteSessionById'),
  controller.deleteSessionById,
);

export default sessionRouter;
