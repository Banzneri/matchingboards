import passport from 'passport';
import { Router } from 'express';
import * as controller from '../controllers/rating.controller.js';
import {
  jwtStrategy,
  passportJwt,
} from '../middleware/passport-jwt.middleware.js';

const ratingRouter = Router({ mergeParams: true });
passport.use(jwtStrategy);

ratingRouter.post('/add/:id',
  passportJwt(),
  controller.validate('addUserRating'),
  controller.addUserRating,
);

export default ratingRouter;
