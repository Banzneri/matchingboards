CREATE TYPE "roleEnum" AS ENUM ('regular', 'admin', 'eventOrganizer');

CREATE TABLE "users"(
  "id" SERIAL PRIMARY KEY,
  "email" VARCHAR UNIQUE NOT NULL,
  "username" VARCHAR UNIQUE NOT NULL,
  "image_url" VARCHAR,
  "description" VARCHAR,
  "salt" VARCHAR NOT NULL,
  "hash" VARCHAR NOT NULL,
  "role" "roleEnum" DEFAULT 'regular',
  "zip_code" INT NOT NULL,
  "latitude" DECIMAL DEFAULT '61.498206',
  "longitude" DECIMAL DEFAULT '23.762262', 
  "created_at" TIMESTAMPTZ NOT NULL DEFAULT Now(),
  "updated_at" TIMESTAMPTZ NOT NULL DEFAULT Now()
);

INSERT INTO "users"(
  "id", 
  "email", 
  "username", 
  "salt", 
  "hash", 
  "zip_code", 
  "latitude", 
  "longitude")
VALUES (
  '0', 
  'anonymous@example.com', 
  'anonymousUser', 
  'salt', 
  'hash', 
  '99998', 
  '0.0', 
  '0.0');

CREATE RULE "protect_anonymous_delete" AS 
ON DELETE TO "users"
WHERE OLD.ID = '0'
DO INSTEAD NOTHING;

CREATE RULE "protect_anonymous_update" AS
ON UPDATE TO "users"
WHERE  OLD.ID = '0'
DO INSTEAD NOTHING;

CREATE TABLE "games" (
  "id" INT PRIMARY KEY,
  "name" VARCHAR NOT NULL,
  "release_year" INT NOT NULL,
  "description" VARCHAR NOT NULL,
  "min_players" INT NOT NULL,
  "max_players" INT NOT NULL,
  "image_url" VARCHAR
);

CREATE TABLE "groups" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR NOT NULL,
  "description" VARCHAR,
  "private" BOOLEAN NOT NULL DEFAULT TRUE,
  "creator_id" INT NOT NULL,
  "created_at" TIMESTAMPTZ NOT NULL DEFAULT Now(),
  CONSTRAINT "creator_id"
  FOREIGN KEY("creator_id")
  REFERENCES "users"("id")
);

CREATE TABLE "sessions" (
  "id" SERIAL PRIMARY KEY,
  "date" DATE NOT NULL,
  "time" TIME NOT NULL,
  "game_id" INT NOT NULL,
  "description" VARCHAR,
  "creator_id" INT NOT NULL,
  "created_at" TIMESTAMPTZ NOT NULL DEFAULT Now(),
  "session_finished" BOOLEAN NOT NULL DEFAULT false,
  "latitude" DECIMAL NOT NULL,
  "longitude" DECIMAL NOT NULL,
  CONSTRAINT "game_id"
  FOREIGN KEY("game_id")
    REFERENCES "games"("id")
    ON DELETE SET NULL
    ON UPDATE CASCADE,	
  CONSTRAINT "creator_id"
  FOREIGN KEY("creator_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE	
);

CREATE TABLE "users_games"(
  "user_id" INT NOT NULL,
  "game_id" INT NOT NULL,
  "owned" BOOLEAN DEFAULT false,
  PRIMARY KEY ("user_id", "game_id"),
  FOREIGN KEY("user_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY("game_id")
    REFERENCES "games"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE	
);

CREATE TABLE "game_votes" (
  "session_id" INT NOT NULL,
  "game_id" INT NOT NULL,
  "voter_id" INT NOT NULL,
  PRIMARY KEY ("session_id", "game_id"),
  CONSTRAINT "session_id"
  FOREIGN KEY("session_id")
    REFERENCES "sessions"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT "game_id"
  FOREIGN KEY("game_id")
    REFERENCES "games"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT "voter_id"
  FOREIGN KEY("voter_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE TYPE "statusEnum" AS ENUM ('pending', 'accepted', 'rejected');

CREATE TABLE "users_sessions"(
  "user_id" INT NOT NULL,
  "session_id" INT NOT NULL,
  "status" "statusEnum" DEFAULT 'accepted',
  PRIMARY KEY ("user_id", "session_id"),
  CONSTRAINT "user_id"
  FOREIGN KEY("user_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT "session_id"
  FOREIGN KEY("session_id")
    REFERENCES "sessions"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE	
);

CREATE TABLE "users_groups"(
  "user_id" INT NOT NULL,
  "group_id" INT NOT NULL,
  PRIMARY KEY ("user_id", "group_id"),
    CONSTRAINT "user_id"
  FOREIGN KEY("user_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT "group_id"
  FOREIGN KEY("group_id")
    REFERENCES "groups"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE	
);

CREATE TABLE "ratings"(
  "value" INT NOT NULL,
  "giver_id" INT NOT NULL DEFAULT '0',
  "receiving_id" INT NOT NULL DEFAULT '0',
  PRIMARY KEY("giver_id", "receiving_id"),
  CONSTRAINT "giver_id"
    FOREIGN KEY("giver_id")
    REFERENCES "users"("id")
    ON DELETE SET DEFAULT
    ON UPDATE CASCADE,
  CONSTRAINT "receiving_id"
    FOREIGN KEY("receiving_id")
    REFERENCES "users"("id")
    ON DELETE SET DEFAULT
    ON UPDATE CASCADE	
);

CREATE TABLE "friends"(
  "sender_id" INT NOT NULL,
  "receiver_id" INT NOT NULL,
  PRIMARY KEY ("sender_id", "receiver_id"),
  "status" "statusEnum" DEFAULT 'pending',
  "invite_sent_at" TIMESTAMPTZ NOT NULL DEFAULT Now(),
  CONSTRAINT "receiver_id"
    FOREIGN KEY("receiver_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT "sender_id"
    FOREIGN KEY("sender_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE "blacklist"(
  "user_id" INT NOT NULL,
  "blacklisted_id" INT NOT NULL,
  PRIMARY KEY ("user_id", "blacklisted_id"),
  CONSTRAINT "user_id"
    FOREIGN KEY("user_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT "blacklisted_id"
    FOREIGN KEY("blacklisted_id")
    REFERENCES "users"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE	
);

CREATE TABLE "messages"(
  "id" SERIAL PRIMARY KEY,
  "message" VARCHAR NOT NULL,
  "sender_id" INT NOT NULL DEFAULT '0',
  "receiver_id" INT NOT NULL DEFAULT '0',
  "sent_at" TIMESTAMPTZ NOT NULL DEFAULT Now(),
  "has_read" BOOLEAN NOT NULL DEFAULT false,
  CONSTRAINT "sender_id"
  FOREIGN KEY("sender_id")
    REFERENCES "users"("id")
    ON DELETE SET DEFAULT
    ON UPDATE CASCADE,
  CONSTRAINT "receiver_id"
  FOREIGN KEY("receiver_id")
    REFERENCES "users"("id")
    ON DELETE SET DEFAULT
    ON UPDATE CASCADE
);

CREATE TABLE "session_messages"(
  "id" SERIAL PRIMARY KEY,
  "message" VARCHAR NOT NULL,
  "sender_id" INT NOT NULL DEFAULT '0',
  "session_id" INT NOT NULL,
  "sent_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  CONSTRAINT "sender_id"
  FOREIGN KEY("sender_id")
    REFERENCES "users"("id")
    ON DELETE SET DEFAULT
    ON UPDATE CASCADE,
  CONSTRAINT "session_id"
  FOREIGN KEY("session_id")
    REFERENCES "sessions"("id")
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

INSERT INTO "users" ("email", "username", "salt", "hash", "zip_code", "latitude", "longitude")
VALUES 
('hungryhippo@example.com', 'HungryHippo', 'f6b994dc884baf1ed61d27331350bf1254c47139cfab54c03a56e3ae3c9e2fa7', '57fd871676d67a3b0dc38dae174278beebf47b4c6c550c9263d1a8a8781c04e57bb7be1159fd67709e94e3b821da9deb13e2a2137c9025981c2b55806dafe6dd', '20540', '61.493975', '23.766992'),
('maliciousmonkey@example.com', 'MaliciousMonkey', 'f7ddb22a4d637e229dc04388304520bb7d77f744524bd42f61f41b0cc0e02dd4', 'd1c3de2f66807783765444e8b84c40b60cc5cb594bd1b623be94e471397089a196c2aef447b9704b0369ff61fdab7438168d1019fbae1eb12998497e7f6d1f37', '20100', '61.377046', '23.777395'),
('racyraccoon@example.com', 'RacyRaccoon', 'bb36fe5bb3c2d9ec7271a6a3d931fd6b25aadb5976f96da2c26441491544d6ea', 'b1cade64df97cfd7fa8424d9ce0b0b99f5c6b3ad2e54ee03fb6aaa0d2c9738fe1dafeb8cd59a3d37a5a22a2fd71be0697ffcabb30258354b3760e4f1d3e4c44a', '96100', '60.996931', '24.463618'),
('flirtyflamingo@example.com', 'FlirtyFlamingo', '855d6e03638912bd3fabc162b3fd559b0289723949da8ee4327fe21afe0a8d8e', 'ace9d365b7d72d3c5c42d070ba843e8a73ebd701cb3b437ae6f78e1412a998b5d63905650e2e477f26a1072d6937153daa81cfc22f4ddb10faed5f42c6eee369', '39100', '60.532191', '24.211656'),
('gloriousgorilla@example.com', 'GloriousGorilla', 'c6ba58889af94d6370663a0978461d54b95941fba3eb023460724fb943839910', '0b5541d4bb8c1bef985aa7537391a6146acefeecdd6506d22eb746c0c154a091b1945736c06f251166a626577467333eed103dfadf97cf429783e5627e298efa', '52100', '60.652395','22.582000')
;

INSERT INTO "games" ("id", "name", "release_year", "description", "min_players", "max_players", "image_url")
VALUES
('2223', 'UNO', '1971', 'Players race to empty their hands and catch opposing players with cards left in theirs, which score points. In turns, players attempt to play a card by matching its color, number, or word to the topmost card on the discard pile. If unable to play, players draw a card from the draw pile, and if still unable to play, they pass their turn. Wild and special cards spice things up a bit.&#10;&#10;UNO is a commercial version of Crazy Eights, a public domain card game played with a standard deck of playing cards.&#10;&#10;This entry includes all themed versions of UNO that do not include new cards.&#10;&#10;', '2', '10', 'https://cf.geekdo-images.com/SU-OL7XWn7BOiSYevyTThw__original/img/JeM6Gys8GmsTTRXcukfk92YtIcA=/0x0/filters:format(jpeg)/pic981505.jpg'),
('193037', 'Dead of Winter: The Long Night', '2016', 'Dead of Winter: The Long Night is a standalone expansion for Dead of Winter: A Crossroads Game that introduces the Raxxon location where horrible experiments will spill out into the town unless players can contain them.&#10;&#10;The game has players at a new colony location trying to survive with new survivors against brand new challenges. Can you handle being raided by members of other colonies? Will you explore more and unravel the mysteries of the Raxxon pharmaceutical location to find powerful items but release stronger enemies? Or will you upgrade your colony to help it better withstand the undead horde? These are all choices you will get to make in this new set, and if you want, you can mix in the survivors and cards from the original set to increase the variety even more.&#10;&#10;Part of the Dead of Winter series.&#10;&#10;', '2', '5', 'https://cf.geekdo-images.com/wpWzYeX4dg5TgNdR9OsxAg__original/img/Cu0M5-DOI29unYaVVm40KDuS-rY=/0x0/filters:format(jpeg)/pic2906832.jpg'),
('40091', 'Monopoly: Turku', '2007', 'A monopoly (from Greek monos &mu;&#207;&#140;&nu;&omicron;&sigmaf; (alone or single) + polein &pi;&omega;&lambda;&epsilon;&#225;&#191;&#150;&nu; (to sell)) exists when a specific person or enterprise is the only supplier of a particular commodity.&#10;&#10;Monopoly is a roll-and-move game where players move around the game board buying or trading properties, developing their properties with houses and hotels. When opponents land on players owned property, the owning player collects rent from the landing player. Owning sets of properties increases the rent paid to the player who owns them, by the player who lands on the square. The ultimate goal is to have the most money and drive opponents into bankruptcy.&#10;&#10;Another version of Monopoly, this time based on the city of Turku (Swedish name &Aring;bo), Finland.&#10;&#10;', '2', '6', 'https://cf.geekdo-images.com/vB1pkn64m1_zj_bAWMtLCw__original/img/NPaLacK3wbTlC6cXz2dLwuD2Gas=/0x0/filters:format(jpeg)/pic568963.jpg'),
('180564', 'Carcassonne: Star Wars', '2015', 'Description from the publisher:&#10;&#10;Carcassonne: Star Wars combines the exciting adventures of the Star Wars universe with the gameplay of Carcassonne, with the known rules of the game being simplified through clever changes that bring an entirely new feel to the game.&#10;&#10;This is similar to Carcassonne but with Roads replaced with Trading Routes and claimed by Merchants instead of Robbers, Cities replaced with Asteroid Fields and claimed by Explorers instead of Knights and Cloisters replaced with Planets and claimed by Conquerers instead of Monks. There is no farming equivalent. Faction symbols (Empire, Rebel Alliance and Bounty Hunters) provide scoring bonuses regardless of what faction your Meeples belong to. Majority control is determined by dice rolling with the highest result rather than the sum determining the winner, although tiebreaks and defeats still provide some points. A player receives dice equal to the number of his Meeples involved in the majority control plus an additional one for using his large Meeple and one for any matching Faction symbols but this is always capped at three dice. One notable change is that Planets can be conquered by placing tiles adjacent to the Planet tile where the player now has the options of placing a Meeple on the adjacent Planet tile or on the tile he placed.&#10;&#10;There is also a four player team variant where it is the two Empire colours (black/Darth Vader and white/Storm Troopers) versus the two Rebel Alliance colours (red/Luke Skywalker and green/Yoda). Orange/Boba Fett is not used.&#10;&#10;', '2', '5', 'https://cf.geekdo-images.com/iW8mD2VT5pCiJ7utrbuIHQ__original/img/IIyY_Ianho-fpkyRTJNAdCsaHko=/0x0/filters:format(jpeg)/pic2602614.jpg')
;

INSERT INTO "users_games" ("user_id", "game_id", "owned")
VALUES 
((SELECT "id" FROM "users" WHERE "username"='HungryHippo'), '40091', false), 
((SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), '193037', false), 
((SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), '2223', true), 
((SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), '193037', true), 
((SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), '40091', true), 
((SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), '180564', true), 
((SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), '2223', true), 
((SELECT "id" FROM "users" WHERE "username"='HungryHippo'), '180564', true)
;

INSERT INTO "sessions" ("date", "time", "game_id", "description", "creator_id", "latitude", "longitude")
VALUES 
('2022-10-1', '06:00', '2223', 'Start your day with UNO in Vorssa', (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), '60.806641','23.617069'),
('2022-06-12', '16:00', '193037', 'Survival battle in Hervanta', (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), '61.448844','23.859130'),
('2022-08-10', '18:00', '40091', 'Monopoly session in student village', (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), '60.462302','22.300667'),
('2022-08-29', '16:30', '180564', 'Kicking your ass in Carcassonne at Espoo IKEA', (SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), '60.216820','24.661238'),
('2022-05-15', '17:00', '2223', 'A friendly game of UNO at Varusteleka', (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), '60.248322','24.841969'),
('2022-10-12', '21:00', '180564', 'Boardgames and chill in Lielahti', (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), '61.515320','23.675858')
;

INSERT INTO "friends" ("sender_id", "receiver_id", "status")
VALUES 
((SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), 'pending'), 
((SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), 'pending'), 
((SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), 'pending'),
((SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), 'pending'),
('2', '4', 'pending')
;

INSERT INTO "users_sessions" ("user_id", "session_id", "status")
VALUES  
((SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT id FROM "sessions" WHERE "description"='Start your day with UNO in Vorssa'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT id FROM "sessions" WHERE "description"='Survival battle in Hervanta'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), (SELECT id FROM "sessions" WHERE "description"='Monopoly session in student village'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), (SELECT id FROM "sessions" WHERE "description"='Kicking your ass in Carcassonne at Espoo IKEA'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT id FROM "sessions" WHERE "description"='A friendly game of UNO at Varusteleka'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT id FROM "sessions" WHERE "description"='Boardgames and chill in Lielahti'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT id FROM "sessions" WHERE "description"='A friendly game of UNO at Varusteleka'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT id FROM "sessions" WHERE "description"='Kicking your ass in Carcassonne at Espoo IKEA'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), (SELECT id FROM "sessions" WHERE "description"='A friendly game of UNO at Varusteleka'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), (SELECT id FROM "sessions" WHERE "description"='Boardgames and chill in Lielahti'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT id FROM "sessions" WHERE "description"='Survival battle in Hervanta'), 'accepted'), 
((SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), (SELECT id FROM "sessions" WHERE "description"='Boardgames and chill in Lielahti'), 'pending'),
((SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT id FROM "sessions" WHERE "description"='Start your day with UNO in Vorssa'), 'pending'),
((SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT id FROM "sessions" WHERE "description"='Survival battle in Hervanta'), 'pending')
;

INSERT INTO "ratings" ("value", "giver_id", "receiving_id")
VALUES 
('5', (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo')), 
('3', (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon')), 
('1', (SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), (SELECT "id" FROM "users" WHERE "username"='HungryHippo')), 
('4', (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT "id" FROM "users" WHERE "username"='HungryHippo')),
('1', (SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey')),
('4', (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey')),
('5', (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey')),
('4', (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo')),
('4', (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT "id" FROM "users" WHERE "username"='GloriousGorilla')),
('4', (SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon')),
('5', (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'))
;

INSERT INTO "messages" ("message", "sender_id", "receiver_id", "sent_at", "has_read")
VALUES
('How are you?', (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), '2022-03-25 15:43:12.007784+02', true),
('Not bad, waiting for the summer', (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), '2022-03-25 15:45:12.007784+02', true),
('Up for a game on Saturday?', (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), '2022-03-25 15:46:12.007784+02', true),
('Yep, see you then', (SELECT "id" FROM "users" WHERE "username"='HungryHippo'), (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), '2022-03-25 15:47:12.007784+02', false),
('Hey you forgot your umbrella at my place!', (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), '2022-03-27 15:43:12.007784+02', true),
('You want me to bring it on Tuesday?', (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), '2022-03-27 15:44:12.007784+02', true),
('Yes please!', (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), '2022-03-27 16:43:12.007784+02', false),
('Do you know if it will rain on Saturday? I forgot my umbrella somewhere...', (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), (SELECT "id" FROM "users" WHERE "username"='GloriousGorilla'), '2022-03-26 15:43:12.007784+02', false)
;

INSERT INTO "session_messages" ("message", "sender_id", "session_id", "sent_at")
VALUES
('Looking forward to tonight!', (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT id FROM "sessions" WHERE "description"='Kicking your ass in Carcassonne at Espoo IKEA'), '2022-03-26 15:43:12.007784+02'),
('I will be a bit late on Sunday', (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT id FROM "sessions" WHERE "description"='Monopoly session in student village'), '2022-03-27 19:43:12.007784+02'),
('No worries!', (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT id FROM "sessions" WHERE "description"='Monopoly session in student village'), '2022-03-27 19:50:12.007784+02'),
('Good! See you then', (SELECT "id" FROM "users" WHERE "username"='RacyRaccoon'), (SELECT id FROM "sessions" WHERE "description"='Monopoly session in student village'), '2022-03-27 19:53:12.007784+02'),
('Never played this game before!', (SELECT "id" FROM "users" WHERE "username"='FlirtyFlamingo'), (SELECT id FROM "sessions" WHERE "description"='A friendly game of UNO at Varusteleka'), '2022-03-22 19:53:12.007784+02'),
('You will love it', (SELECT "id" FROM "users" WHERE "username"='MaliciousMonkey'), (SELECT id FROM "sessions" WHERE "description"='A friendly game of UNO at Varusteleka'), '2022-03-27 20:20:12.007784+02')
;