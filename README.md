# Matching boards

MatchingBoards is an app for looking company to play board games with. Users can make an account for themselves, customize their profile and add games they own and games they wish to play to their respective lists. Users can also create sessions for the games that they own to find company to play with. Users can find open sessions by the distance they are willing to travel from their location. Further discussion about the incoming session can be done in the session chat that will open to the users that have joined the session.
Further development ideas include:

- Private sessions, where the host could invite their friends only and this session would not appear in the search to other users.
- Private messaging between friends.
- Users could create friend groups and have private chat inside the group to faster connect with other users to make an agreement about gaming session.
- User rating where users who were in the same session could give ratings to each other after the session has ended.
- Blacklisting users for security purposes.
- Public places like libraries, coffee shops and game stores could make their own Event Organizer accounts and could create events similar to sessions.
- Users could make sales announcement of a game they own and want to sell and other users would be able to view and browse these sales announcements.
- A user could be promoted to Admin status.

You can view the app by cloning the repository and running the following commands in terminal:

In server folder:
- npm run db
- npm run dev

In frontend folder:
- npm start

There are five mock users already made and you can log in with those accounts:

- hungryhippo@example.com
- maliciousmonkey@example.com
- racyraccoon@example.com
- flirtyflamingo@example.com
- gloriousgorilla@example.com

All of these mock users have the same password:
Password1!

![image](https://drive.google.com/uc?export=view&id=13y08NCPP2JB-4IpUiRlPlDal-ITgdpiN)
![image](https://drive.google.com/uc?export=view&id=1OARr15p0AsBHi41GuyKkbCmYydckJ9rf)
![image](https://drive.google.com/uc?export=view&id=1Gim1bssE31Eu1NydHenNZ30rq2v7oq68)
![image](https://drive.google.com/uc?export=view&id=1boYAC4vv3wl44aCtSYto8lk1M1mFWCIv)
![image](https://drive.google.com/uc?export=view&id=1sRExBjoKgcTAfOvttG2Oiagx_cNUHTvL)

Endpoint can be found in our [wiki] (https://gitlab.com/Banzneri/matchingboards/-/wikis/home)
