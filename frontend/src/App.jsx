import './App.css';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider, GlobalStyles } from '@mui/material';
import Router from './routes/Routes';
import theme from './assets/Themes';
import bg from './assets/stone-bg2.jpg';
import Navbar from './components/Navbar';
import { UserProvider } from './contexts/UserContext';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles
        styles={{
          body: {
            backgroundImage: `url(${bg})`,
            backgroundSize: 'cover',
            backgroundAttachment: 'fixed',
          },
        }}
      />
      <BrowserRouter>
        <div>
          <UserProvider>
            <Navbar />
            <Router />
          </UserProvider>
        </div>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
