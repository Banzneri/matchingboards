export const paperStyle = {
  padding: 30,
  height: 'auto',
  borderRadius: '15px',
  width: 420,
  margin: '20px auto',
};

export const altStyle = {
  backgroundColor: 'rgba(52, 45, 43, 0.959)',
  color: 'white',
  margin: '20px auto',
  borderRadius: '15px',
  padding: 30,
};

export const contentBox = {
  background: 'rgba(228, 212, 179, 1)',
  padding: '0.5rem 1rem 0.5rem 1rem',
  color: 'black',
  borderRadius: '15px',
};

export const contentColor = {
  background: 'rgba(228, 212, 179, 0.824)',
};

export const notiStyle = {
  padding: 10,
  height: 'auto',
  borderRadius: '80px',
  width: 'auto',
  maxWidth: '800px',
  minWidth: '400px',
  margin: 'auto',
  backgroundColor: 'rgba(255, 214, 161, 0.959)',
  display: 'flex',
  alignItems: 'center',
  textAlign: 'center',
  justifyContent: 'center',
};

export const nullStyle = {
  padding: 10,
  height: 'auto',
  borderRadius: '80px',
  width: 'auto',
  maxWidth: '300px',
  minWidth: '200px',
  margin: 'auto',
  backgroundColor: 'rgba(255, 214, 161, 0.959)',
  display: 'flex',
  justifyContent: 'center',
};
