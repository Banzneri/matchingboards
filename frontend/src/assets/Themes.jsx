import { createTheme } from '@mui/material';

const theme = createTheme({
  palette: {
    primary: {
      main: '#51514f',
    },
    secondary: {
      main: '#342d2b',
    },
    beige: {
      main: '#e4d4b3',
    },
    steel: {
      main: '#848482',
    },
  },
  typography: {
    beige: {
      color: '#e4d4b3',
    },
    coal: {
      color: '#342d2b',
    },
    white: {
      color: '#ffff',
    },
    fontFamily: 'Lora',
  },
  listItem: {
    color: '#ffff',
    padding: '0px',
    width: '25ch',
  },
  paperStyle: {
    padding: 30,
    height: 'auto',
    borderRadius: '15px',
    width: 420,
    margin: '20px auto',
    backgroundColor: 'rgba(52, 45, 43, 0.959)',
    color: 'white',
  },
  selectInput: {
    textAlign: 'center',
    backgroundColor: 'rgba(228, 212, 179, 0.824)',
    borderRadius: '5px',
    marginTop: '30px',
    justifyContent: 'center',
    height: '4rem',
    width: '26rem',
  },
  centeredBox: {
    display: 'flex',
    justifyContent: 'center',
  },
  dateTimeInput: {
    backgroundColor: 'rgba(228, 212, 179, 0.824)',
    marginTop: '30px',
    borderRadius: '5px',
  },
  altStyle: {
    backgroundColor: 'rgba(52, 45, 43, 0.959)',
    color: 'white',
    margin: '20px auto',
    borderRadius: '15px',
    padding: 30,
  },
  contentBox: {
    background: 'rgba(228, 212, 179, 1)',
    padding: '0.5rem 1rem 0.5rem 1rem',
    color: 'black',
    borderRadius: '15px',
  },
});

export default theme;
