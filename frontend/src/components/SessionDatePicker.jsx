import { useState } from 'react';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import { Box, TextField } from '@mui/material';
import theme from '../assets/Themes';

export default function SessionDatePicker({ setSelectedDate }) {
  const today = new Date();
  const [value, setValue] = useState({});

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Box style={theme.centeredBox}>
        <DatePicker
          minDate={today}
          inputFormat="dd/MM/yyyy"
          label="Pick a date, any date"
          value={value}
          onChange={(newValue) => {
            const checkMonth =
              (newValue.getMonth() + 1 < 10 ? '0' : '') +
              (newValue.getMonth() + 1);
            const checkDate =
              (newValue.getDate() < 10 ? '0' : '') + newValue.getDate();
            const selectedDate = `${newValue.getFullYear()}-${checkMonth}-${checkDate}`;
            setValue(newValue);
            setSelectedDate(selectedDate);
          }}
          renderInput={(params) => (
            <TextField
              style={theme.dateTimeInput}
              variant="filled"
              {...params}
            />
          )}
        />
      </Box>
    </LocalizationProvider>
  );
}
