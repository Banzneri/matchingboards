import React, { useState, useEffect } from 'react';
import { Typography, Button, Box, Paper, Snackbar } from '@mui/material';
import { PersonAdd } from '@mui/icons-material';
import axios from 'axios';
import { notiStyle } from '../../styles';
import { useUser } from '../../contexts/UserContext';

const FriendRequest = ({ senderId, updateNotifications }) => {
  const [open, setOpen] = useState(false);
  const [snackbarMsg, setSnackbarMsg] = useState();
  const [friendUsername, setFriendUsername] = useState();

  const { token } = useUser();

  useEffect(() => {
    axios
      .get(`http://localhost:3001/users/${senderId}`)
      .then((response) => {
        setFriendUsername(response.data.userName);
      })
      .catch(() => {
        setSnackbarMsg(`Error finding usernames.`);
        setOpen(true);
      });
  }, []);

  const responseToFriendRequest = (isAccepted) => {
    const statusMsg = isAccepted ? 'accepted' : 'rejected';
    const snackbarMessage = isAccepted
      ? 'Friend request accepted'
      : 'Friend request rejected';
    axios
      .patch(
        `http://localhost:3001/friends/verify/`,
        { senderId, status: statusMsg },
        {
          headers: { Authorization: token },
        },
      )
      .then(() => {
        setSnackbarMsg(snackbarMessage);
        setOpen(true);
        updateNotifications();
      })
      .catch(() => {
        setSnackbarMsg(`Error verifying the friend request.`);
        setOpen(true);
      });
  };

  const handleAcceptClick = () => {
    responseToFriendRequest(true);
  };

  const handleRejectClick = () => {
    responseToFriendRequest(false);
  };

  const handleClose = () => {
    setOpen(true);
  };

  return (
    <div style={{ margin: 15 }}>
      <Paper style={notiStyle}>
        <PersonAdd style={{ height: '30px', width: '30px' }} />
        <Typography variant="h6" style={{ margin: 10 }}>
          You have a friend request from the user {friendUsername}.
        </Typography>
        <Box style={{ display: 'flex' }}>
          <Button
            variant="contained"
            color="success"
            style={{ margin: 10 }}
            onClick={handleAcceptClick}
          >
            Accept
          </Button>
          <Button
            variant="contained"
            color="error"
            style={{ margin: 10 }}
            onClick={handleRejectClick}
          >
            Reject
          </Button>
        </Box>
        <Snackbar
          open={open}
          autoHideDuration={5000}
          onClose={handleClose}
          message={snackbarMsg}
        />
      </Paper>
    </div>
  );
};

export default FriendRequest;
