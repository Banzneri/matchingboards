import { Button, Typography } from '@mui/material';
import axios from 'axios';
import React from 'react';
import { useUser } from '../../contexts/UserContext';

function NonActiveButton({ handleLeave }) {
  return (
    <Button
      onClick={handleLeave}
      style={{
        width: '100%',
        margin: 0,
        borderRadius: 0,
      }}
      color="secondary"
      variant="contained"
    >
      <Typography>Leave</Typography>
    </Button>
  );
}

function ActiveButton({ handleJoin }) {
  return (
    <Button
      onClick={handleJoin}
      style={{
        width: '100%',
        margin: 0,
        borderRadius: 0,
      }}
      color="success"
      variant="contained"
    >
      Join
    </Button>
  );
}

function JoinSessionButton({
  joined,
  session,
  setJoined,
  setOpen,
  setMessage,
}) {
  const { currentUser, token } = useUser();

  const handleJoin = async () => {
    try {
      await axios.post(
        'http://localhost:3001/sessions/join',
        { sessionId: session.id },
        {
          headers: { Authorization: token },
        },
      );
      setJoined(true);
    } catch (error) {
      setOpen(true);
      setMessage(error.response.data);
    }
  };

  const handleLeave = async () => {
    try {
      await axios.delete('http://localhost:3001/sessions/users', {
        data: {
          sessionId: session.id,
          userId: currentUser.id,
        },
        headers: { Authorization: token },
      });
      setJoined(false);
    } catch (error) {
      setOpen(true);
      setMessage(error.response.data);
    }
  };

  return joined ? (
    <NonActiveButton handleLeave={handleLeave} />
  ) : (
    <ActiveButton handleJoin={handleJoin} />
  );
}

export default JoinSessionButton;
