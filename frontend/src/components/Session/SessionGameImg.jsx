import { Avatar } from '@mui/material';
import React from 'react';
import DefaultGameImage from '../Game/DefaultGameImage';

function SessionGameImg({ src }) {
  if (!src) return <DefaultGameImage />;

  return (
    <Avatar
      sx={{
        backgroundColor: 'crimson',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '10rem',
        height: 'auto',
      }}
      src={src}
      variant="square"
    />
  );
}

export default SessionGameImg;
