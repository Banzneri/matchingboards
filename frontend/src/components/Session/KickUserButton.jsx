import { Button } from '@mui/material';
import React from 'react';

function KickUserButton({ text, handleKickUser }) {
  return (
    <Button
      onClick={handleKickUser}
      style={{
        padding: 0,
        fontSize: '10px',
      }}
      color="primary"
      variant="contained"
      type="submit"
    >
      {text}
    </Button>
  );
}

export default KickUserButton;
