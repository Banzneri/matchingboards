import React, { useEffect, useState } from 'react';
import { Grid, Snackbar, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { contentBox } from '../../styles';
import JoinSessionButton from './JoinSessionButton';
import { useUser } from '../../contexts/UserContext';
import DefaultGameImage from '../Game/DefaultGameImage';

function SessionCard({ session, profile }) {
  const [hover, setHover] = useState(false);
  const [joined, setJoined] = useState(false);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState('');
  const [participants, setParticipants] = useState({ current: 0, max: 0 });

  const { token, currentUser } = useUser();

  const updateParticipants = async () => {
    try {
      const participantsResponse = await axios.get(
        `http://localhost:3001/sessions/participants/${session.id}`,
        { headers: { Authorization: token } },
      );
      const participantsIncludingHost = participantsResponse.data.length + 1;
      setParticipants({
        current: participantsIncludingHost,
        max: session.game.maxPlayers,
      });
    } catch (error) {
      setOpen(true);
      setMessage(error.response.data);
    }
  };

  useEffect(() => {
    const update = async () => {
      try {
        const userSessions = await axios.get(
          `http://localhost:3001/sessions/user/${currentUser.id}`,
          { headers: { Authorization: token } },
        );
        const joinedSession = userSessions.data.find(
          (userSession) => userSession.id === session.id,
        );

        setJoined(!!joinedSession);
      } catch (error) {
        setOpen(true);
        setMessage(error.response.data);
      }
    };
    update();
  }, []);

  useEffect(() => {
    updateParticipants();
  }, [joined]);

  const toggleHover = () => {
    setHover(!hover);
  };

  const { game, host } = session;

  return (
    <Grid container justifyContent="center" sx={{ marginBottom: '2rem' }}>
      <Grid item md={profile ? 8 : 6} xs={12}>
        <Grid
          container
          sx={{ ...contentBox, borderRadius: 0, padding: 0, margin: 0 }}
        >
          <Grid item md={4}>
            <Link
              to="/session"
              state={session}
              style={{ textDecoration: 'none' }}
            >
              {game.imageUrl ? (
                <img
                  style={{
                    width: '100%',
                    height: '100%',
                    objectFit: 'cover',
                  }}
                  src={game.imageUrl}
                  alt="game"
                />
              ) : (
                <DefaultGameImage />
              )}
            </Link>
          </Grid>
          <Grid item md={4} xs={6} sx={{ padding: '0 0.4rem 0 0.4rem' }}>
            <Typography>
              <b>Game</b>: {game.name}
            </Typography>
            <Typography>
              <b>Host</b>:{' '}
              <Link
                style={{
                  color: 'rgba(52, 45, 43, 0.959)',
                  textDecoration: hover ? 'underline' : 'none',
                }}
                to={`/profile/${host.id}`}
                onMouseEnter={toggleHover}
                onMouseLeave={toggleHover}
              >
                <b>{host.userName}</b>
              </Link>
            </Typography>
            <Typography>
              <b>Date</b>: {session.date}
            </Typography>
            <Typography>
              <b>Participants</b>:{' '}
              {`${participants.current} / ${participants.max}`}
            </Typography>
          </Grid>
          <Grid
            item
            sx={{
              borderLeft: '1px gray solid',
              padding: '0 0.4rem 0 0.4rem',
            }}
            md={4}
            xs={6}
          >
            <Typography sx={{ justifySelf: 'center' }} variant="body1">
              <b>Description</b>
            </Typography>
            <Typography variant="body2">{session.description}</Typography>
          </Grid>
          <JoinSessionButton
            joined={joined}
            session={session}
            setJoined={setJoined}
            setOpen={setOpen}
            setMessage={setMessage}
          />
        </Grid>
      </Grid>
      <Snackbar
        open={open}
        autoHideDuration={2500}
        onClose={() => setOpen(false)}
        message={message}
      />
    </Grid>
  );
}

export default SessionCard;
