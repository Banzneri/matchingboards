import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { Fab } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import CreateSessionForm from './CreateSessionForm';
import theme from '../../assets/Themes';

export default function CreateSessionModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Box sx={{ '& > :not(style)': { m: 1 } }} style={theme.centeredBox}>
        <Fab color="primary" aria-label="add" onClick={handleOpen}>
          <AddIcon />
        </Fab>
      </Box>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box>
          <CreateSessionForm handleClosing={setOpen} />
        </Box>
      </Modal>
    </div>
  );
}
