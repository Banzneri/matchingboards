import { Grid, ListItem, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import KickUserButton from './KickUserButton';

const MemberListItem = ({ user, isHost, handleKickUser }) => (
  <ListItem
    key={user.id}
    style={{
      justifyContent: 'center',
      display: 'flex',
      alignItems: 'stretch',
    }}
  >
    <Grid container spacing={2}>
      <Grid
        item
        xs
        style={{
          textAlign: 'right',
        }}
      >
        {null}
      </Grid>
      <Grid item xs={4}>
        <Typography
          variant="h6"
          component={Link}
          to={`/profile/${user.id}`}
          style={{
            textDecoration: 'none',
            color: 'white',
          }}
        >
          {user.userName}
        </Typography>
      </Grid>
      <Grid
        item
        xs
        style={{
          textAlign: 'right',
          alignSelf: 'flex-end',
          marginBottom: 7,
        }}
      >
        {isHost ? (
          <KickUserButton
            handleKickUser={() => handleKickUser(user)}
            text="kick user"
          />
        ) : null}
      </Grid>
    </Grid>
  </ListItem>
);

export default MemberListItem;
