import { React, useState, useEffect } from 'react';
import { Box, Button, Paper, Snackbar, Typography } from '@mui/material';
import axios from 'axios';
import theme from '../../assets/Themes';
import SelectInput from '../SelectInput';
import SessionDatePicker from '../SessionDatePicker';
import SessionTimePicker from '../SessionTimePicker';
import MultilineInputForCreateSessionForm from '../MultilineInputForCreateSessionForm';
import { useUser } from '../../contexts/UserContext';

function PlainCreateSessionForm({ handleClosing }) {
  const [error, setError] = useState('');
  const [message, setMessage] = useState('');
  const [gameList, setGameList] = useState([]);
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const { currentUser, token } = useUser();
  const { id, longitude, latitude } = currentUser;

  const [gameId, setGameId] = useState('');
  const [description, setDescription] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');

  const handleCloseSnackbar = () => {
    setOpenSnackbar(false);
  };

  useEffect(() => {
    const getOwnedGames = () => {
      axios
        .get(`http://localhost:3001/games/users/${id}?owned=true`)
        .then((response) => {
          setGameList(response.data);
        })
        .catch((err) => {
          setError(err.response.data);
          setOpenSnackbar(true);
        });
    };
    getOwnedGames();
  }, []);

  const dropdownInputs = () => {
    if (gameList.length === 0) {
      return null;
    }
    return gameList.map((game) => (
      <option key={game.id} value={game.id}>
        {game.name}
      </option>
    ));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    return axios
      .post(
        'http://localhost:3001/sessions',
        {
          gameId,
          description,
          date,
          time,
          longitude,
          latitude,
        },
        {
          headers: {
            Authorization: token,
          },
        },
      )
      .then(() => {
        setMessage('Session created!');
        setOpenSnackbar(true);
        setTimeout(() => {
          handleClosing(false);
        }, 2000);
      })
      .catch((err) => {
        if (err.response.data.message) {
          setError('Invalid time');
        } else {
          setError(err.response.data);
        }
        setOpenSnackbar(true);
      });
  };

  return (
    <div>
      <Paper elevation={10} style={theme.paperStyle}>
        <Box style={{ paddingBottom: '15px' }}>
          <Typography variant="h4" style={{ textAlign: 'center' }}>
            Create Session
          </Typography>
        </Box>
        <form onSubmit={handleSubmit}>
          <Box style={theme.centeredBox}>
            <SelectInput
              name="gameId"
              style={theme.selectInput}
              value={gameId}
              onChange={(e) => setGameId(e.target.value)}
            >
              <option value="">Choose Your Game</option>
              {dropdownInputs()}
            </SelectInput>
          </Box>
          <MultilineInputForCreateSessionForm
            label="Description"
            name="description"
            id="Description"
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          <SessionDatePicker
            name="date"
            value={date}
            setSelectedDate={(selectedDate) => setDate(selectedDate)}
          />
          <SessionTimePicker
            name="time"
            value={time}
            setSelectedTime={(selectedTime) => setTime(selectedTime)}
          />
          <br />
          <Box style={{ textAlign: 'center' }}>
            <Button type="submit" variant="contained">
              Create Session
            </Button>
          </Box>
        </form>
        <Snackbar
          open={openSnackbar}
          autoHideDuration={5000}
          onClose={handleCloseSnackbar}
          message={error || message}
        />
      </Paper>
    </div>
  );
}

export default PlainCreateSessionForm;
