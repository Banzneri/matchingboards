import { Button } from '@mui/material';
import React from 'react';

function CancelSessionButton({ text }) {
  const handleCancelSession = () => {
    // TODO: Add functionality to the button
  };
  return (
    <Button
      onClick={handleCancelSession}
      style={{
        width: '100%',
        margin: 0,
        borderRadius: 0,
      }}
      color="error"
      variant="contained"
      type="submit"
    >
      {text}
    </Button>
  );
}

export default CancelSessionButton;
