import { Grid, ListItem, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

const SessionHost = ({ sessionInfo }) => (
  <ListItem
    key={sessionInfo.host?.id}
    style={{
      justifyContent: 'center',
      display: 'flex',
      alignItems: 'stretch',
    }}
  >
    <Grid container spacing={2}>
      <Grid
        item
        xs
        style={{
          textAlign: 'right',
          alignSelf: 'flex-end',
          marginBottom: 7,
        }}
      >
        <i
          className="fa-solid fa-crown"
          style={{ color: 'rgba(228, 212, 179)' }}
        />
      </Grid>
      <Grid item xs={4}>
        <Typography
          variant="h6"
          component={Link}
          to={`/profile/${sessionInfo.host?.id}`}
          style={{
            textDecoration: 'none',
            color: 'white',
          }}
        >
          {sessionInfo.host?.userName}
        </Typography>
      </Grid>
      <Grid
        item
        xs
        style={{
          textAlign: 'right',
        }}
      >
        {null}
      </Grid>
    </Grid>
  </ListItem>
);

export default SessionHost;
