import { Container, Typography as Text } from '@mui/material';
import React from 'react';
import SessionCard from './SessionCard';

function SessionList({ sessions, profile }) {
  if (sessions.length === 0) return <Text>Nothing yet...</Text>;

  return (
    <Container direction="column" sx={{ textAlign: 'left' }}>
      {sessions.map((session) => (
        <SessionCard key={session.id} session={session} profile={profile} />
      ))}
    </Container>
  );
}

export default SessionList;
