import React from 'react';
import { Typography as Text } from '@mui/material';
import theme from '../../assets/Themes';

function SessionInfo({ sessionInfo }) {
  return (
    <>
      <Text sx={{ textAlign: 'center' }} variant="h5">
        Game name
      </Text>
      <Text sx={{ textAlign: 'center' }} style={theme.contentBox}>
        {sessionInfo.game?.name}
      </Text>
      <br />
      <Text sx={{ textAlign: 'center' }} variant="h5">
        Play date
      </Text>
      <Text sx={{ textAlign: 'center' }} style={theme.contentBox}>
        {sessionInfo?.date}
      </Text>
      <br />
      <Text sx={{ textAlign: 'center' }} variant="h5">
        Play time
      </Text>
      <Text sx={{ textAlign: 'center' }} style={theme.contentBox}>
        {sessionInfo?.time}
      </Text>
      <br />
      <Text sx={{ textAlign: 'center' }} variant="h5">
        Description
      </Text>
      <Text sx={{ textAlign: 'center' }} style={theme.contentBox}>
        {sessionInfo?.description}
      </Text>
    </>
  );
}

export default SessionInfo;
