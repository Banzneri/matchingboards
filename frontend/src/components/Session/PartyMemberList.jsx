import { List } from '@mui/material';
import MemberListItem from './MemberListItem';
import SessionHost from './SessionHost';

const PartyMemberList = ({
  sessionInfo,
  isHost,
  partyMembers,
  handleKickUser,
}) => {
  const partyMemberListItems = () => {
    if (partyMembers.length === 0) {
      return null;
    }
    return partyMembers.map((user) => (
      <MemberListItem
        key={user.id}
        user={user}
        isHost={isHost}
        handleKickUser={handleKickUser}
      />
    ));
  };

  return (
    <List style={{ width: '100%' }}>
      <SessionHost sessionInfo={sessionInfo} />
      {partyMemberListItems()}
    </List>
  );
};

export default PartyMemberList;
