import { useEffect, useState } from 'react';
import axios from 'axios';
import { Box, Divider, Typography as Text } from '@mui/material';
import { useUser } from '../../contexts/UserContext';
import SessionList from './SessionList';

function OwnSessions({ user }) {
  const [joinedSessions, setJoinedSessions] = useState([]);
  const [hostedSessions, setHostedSessions] = useState([]);

  const { token } = useUser();

  useEffect(() => {
    const update = async () => {
      const sessionsResponse = await axios.get(
        `http://localhost:3001/sessions/user/${user.id}`,
        { headers: { Authorization: token } },
      );
      const sessions = sessionsResponse.data;

      setJoinedSessions(
        sessions.filter((session) => session.host.id !== user.id),
      );
      setHostedSessions(
        sessions.filter((session) => session.host.id === user.id),
      );
    };
    update();
  }, [user]);

  return (
    <>
      <Box>
        <Text sx={{ textAlign: 'center' }} variant="h4">
          Joined sessions
        </Text>
        <Divider sx={{ background: 'grey', marginTop: '0.3rem' }} />
        <br />
        <SessionList sessions={joinedSessions} profile />
      </Box>
      <Box>
        <Text sx={{ textAlign: 'center' }} variant="h4">
          Hosted sessions
        </Text>
        <Divider sx={{ background: 'grey', marginTop: '0.3rem' }} />
        <br />
        <SessionList sessions={hostedSessions} profile />
      </Box>
    </>
  );
}

export default OwnSessions;
