import { Box, TextField } from '@mui/material';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { contentBox } from '../../styles';
import ChatMessage from './ChatMessage';

function SessionChat({
  currentUser,
  token,
  setErrorMessage,
  setOpen,
  session,
}) {
  const [messages, setMessages] = useState([]);
  const [message, setMessage] = useState('');

  useEffect(() => {
    const update = async () => {
      try {
        const messagesResponse = await axios.get(
          `http://localhost:3001/messages/session/${session.id}`,
          {
            headers: {
              Authorization: token,
            },
          },
        );
        setMessages(messagesResponse.data);
      } catch (error) {
        setErrorMessage(error.response.data);
        setOpen(true);
      }
    };
    update();
    const interval = setInterval(() => {
      update();
    }, 10000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const sendMessage = async (event) => {
    event.preventDefault();
    try {
      const sentMessageResponse = await axios.post(
        `http://localhost:3001/messages/session/${session.id}`,
        {
          message,
        },
        {
          headers: {
            Authorization: token,
          },
        },
      );
      const sentMessage = sentMessageResponse.data;
      const { id, sentAt } = sentMessage;
      setMessages([
        {
          id,
          message,
          senderUser: currentUser,
          sentAt,
        },
        ...messages,
      ]);
      setMessage('');
    } catch (error) {
      setErrorMessage(error.response.data);
      setOpen(true);
    }
  };

  return (
    <Box sx={contentBox}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column-reverse',
          height: '400px',
          overflowY: 'auto',
        }}
      >
        {messages.length > 0 &&
          messages.map((msg) => (
            <ChatMessage key={msg.id} msg={msg} currentUser={currentUser} />
          ))}
      </Box>
      <form onSubmit={sendMessage}>
        <TextField
          variant="filled"
          className="text-input"
          label="send"
          style={{
            backgroundColor: 'rgba(241, 233, 217, 1)',
            borderRadius: '5px',
            marginTop: '30px',
          }}
          fullWidth
          type="text"
          onChange={(e) => setMessage(e.target.value)}
          value={message}
        />
      </form>
    </Box>
  );
}

export default SessionChat;
