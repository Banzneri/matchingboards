import {
  Checkbox,
  Container,
  FormControlLabel,
  Slider,
  Snackbar,
  styled,
  Typography,
} from '@mui/material';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { altStyle } from '../../styles';
import SessionList from './SessionList';
import { useUser } from '../../contexts/UserContext';

function valuetext(value) {
  return `${value}km`;
}

const StyledSlider = styled(Slider)(() => ({
  width: '50%',
  color: 'white',
  marginRight: '2rem',
  '& .MuiSlider-markLabel': {
    color: 'white',
  },
}));

const marks = [
  {
    value: 10,
    label: '10km',
  },
  {
    value: 50,
    label: '50km',
  },
  {
    value: 100,
    label: '100km',
  },
  {
    value: 150,
    label: '150km',
  },
  {
    value: 200,
    label: '200km',
  },
];

function SessionListContainer() {
  const [sessions, setSessions] = useState([]);
  const [distance, setDistance] = useState(200);
  const [onlyFriends, setOnlyFriends] = useState(false);
  const [message, setMessage] = useState('');
  const [open, setOpen] = useState(false);

  const { token } = useUser();

  useEffect(() => {
    const getSessions = async () => {
      try {
        const fetchedSessions = await axios.get(
          `http://localhost:3001/sessions/nearby?&distance=${distance}&onlyFriends=${onlyFriends}`,
          { headers: { Authorization: token } },
        );
        setSessions(fetchedSessions.data);
      } catch (error) {
        setOpen(true);
        setMessage(error.response.data);
      }
    };

    getSessions();
  }, [distance, onlyFriends]);

  const onDistanceChange = (event, value) => {
    setDistance(value);
  };

  const onOnlyFriendsChange = (event, value) => {
    setOnlyFriends(value);
  };

  return (
    <Container md={6} style={{ textAlign: 'center', ...altStyle }}>
      <Typography variant="h3">Sessions</Typography>
      <br />
      <Typography>Distance (km)</Typography>
      <StyledSlider
        aria-label="Always visible"
        defaultValue={200}
        getAriaValueText={valuetext}
        step={10}
        marks={marks}
        min={10}
        max={200}
        onChange={onDistanceChange}
      />
      <FormControlLabel
        control={<Checkbox onChange={onOnlyFriendsChange} color="success" />}
        label="Only friends"
        style={{ paddingBottom: '3.8rem' }}
      />
      <SessionList sessions={sessions} />
      <Snackbar
        open={open}
        autoHideDuration={2500}
        onClose={() => setOpen(false)}
        message={message}
      />
    </Container>
  );
}

export default SessionListContainer;
