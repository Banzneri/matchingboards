import { Container, Divider, Grid, Typography as Text } from '@mui/material';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import theme from '../../assets/Themes';
import SessionGameImg from './SessionGameImg';
import JoinSessionButton from './JoinSessionButton';
import CancelSessionButton from './CancelSessionButton';
import PartyMemberList from './PartyMemberList';
import SessionInfo from './SessionInfo';
import SessionChat from './SessionChat';

function SessionInfoView({
  currentUser,
  token,
  setErrorMessage,
  setOpen,
  session,
}) {
  const [partyMembers, setPartyMembers] = useState([]);
  const [isPartyMember, setIsPartyMember] = useState(false);

  const isHost = currentUser.id === session.host?.id;
  const maxParticipants = session.game.maxPlayers;
  const currentParticipants = partyMembers.length + 1;

  const updatePartyMembers = () => {
    axios
      .get(`http://localhost:3001/sessions/participants/${session.id}`, {
        headers: {
          Authorization: token,
        },
      })
      .then((response) => {
        const participants = response.data;
        setPartyMembers(participants);

        if (participants.length !== 0) {
          setIsPartyMember(
            !!participants.find((member) => member.id === currentUser.id),
          );
        }
      })
      .catch(() => {
        setErrorMessage(`Error finding usernames.`);
        setOpen(true);
      });
  };

  useEffect(() => {
    updatePartyMembers();
  }, []);

  const handleJoinButton = (joined) => {
    setIsPartyMember(joined);
    updatePartyMembers();
  };

  const handleKickUser = (user) => {
    axios
      .delete('http://localhost:3001/sessions/users/', {
        headers: {
          Authorization: token,
        },
        data: {
          userId: user.id,
          sessionId: session.id,
        },
      })
      .then(() => {
        setErrorMessage(`User kicked`);
        setOpen(true);
        updatePartyMembers();
      })
      .catch((error) => {
        setErrorMessage(error.response.data);
        setOpen(true);
      });
  };

  return (
    <Container style={theme.altStyle}>
      <Grid container spacing={4}>
        <Grid item md={4} xs={12}>
          <Text sx={{ textAlign: 'center' }} variant="h2">
            Quest log
          </Text>
          <br />
          <SessionGameImg src={session.game?.imageUrl} />
          <br />
          <SessionInfo sessionInfo={session} />
          <br />
          {isHost ? (
            <CancelSessionButton text="cancel quest" />
          ) : (
            <JoinSessionButton
              joined={isPartyMember}
              session={session}
              setJoined={handleJoinButton}
              setOpen={setOpen}
              setMessage={setErrorMessage}
            />
          )}
        </Grid>
        <Grid item md={8} xs={12}>
          <br />
          <br />
          <Text sx={{ textAlign: 'center' }} variant="h4">
            The Party {currentParticipants}/{maxParticipants}
          </Text>
          <Divider sx={{ background: 'grey', marginTop: '0.3rem' }} />
          <br />
          <Grid item>
            <PartyMemberList
              partyMembers={partyMembers}
              sessionInfo={session}
              isHost={isHost}
              isPartyMember={isPartyMember}
              handleKickUser={handleKickUser}
            />
          </Grid>
          <br />
          <br />
          {isHost || isPartyMember ? (
            <>
              <Text sx={{ textAlign: 'center' }} variant="h5">
                Party Chat
              </Text>
              <br />
              <SessionChat
                currentUser={currentUser}
                token={token}
                setErrorMessage={setErrorMessage}
                setOpen={setOpen}
                session={session}
              />
            </>
          ) : null}
        </Grid>
      </Grid>
    </Container>
  );
}

export default SessionInfoView;
