import { Box, Typography as Text } from '@mui/material';
import { Link } from 'react-router-dom';
import { contentBox } from '../../styles';

function ChatMessage({ msg, currentUser }) {
  const formatDate = (date) => {
    const hours =
      date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
    const minutes =
      date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    return `${`${[date.getDate(), date.getMonth() + 1, date.getFullYear()].join(
      '/',
    )} ${hours}`}:${minutes}`;
  };

  const ownMessage = (message) => message.senderUser.id === currentUser.id;

  return (
    <Box key={msg.id} sx={{ margin: '1rem' }}>
      <Link to={`/profile/${msg.senderUser.id}`}>
        <Text
          sx={{
            marginLeft: '0.5rem',
            fontWeight: 'bold',
            color: 'black',
            textDecoration: 'none',
          }}
        >
          {ownMessage(msg) ? 'You' : msg.senderUser.userName}
        </Text>
      </Link>
      <Text
        style={{
          ...contentBox,
          background: 'rgba(255, 255, 255, 0.3)',
          margin: '0.2rem',
        }}
      >
        {msg.message}
      </Text>
      <Text sx={{ marginLeft: '0.5rem' }} variant="subtitle2">
        {formatDate(new Date(msg.sentAt))}
      </Text>
    </Box>
  );
}

export default ChatMessage;
