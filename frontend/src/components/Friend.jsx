import React from 'react';
import {
  Typography,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
} from '@mui/material';
import { Link } from 'react-router-dom';

const Friend = ({ friend }) => (
  <Grid item key={friend.id} style={{ textAlign: 'center' }}>
    <Grid container spacing={2}>
      <Grid item xs={12} md={6}>
        <List>
          <Link
            to={`/profile/${friend.id}`}
            style={{ textDecoration: 'none', color: 'white' }}
          >
            <ListItem>
              <ListItemAvatar />
              <Avatar
                src={friend.imageUrl}
                variant="rounded"
                style={{ margin: 10 }}
              />
              <Typography variant="h5">{friend.userName}</Typography>
            </ListItem>
          </Link>
        </List>
      </Grid>
    </Grid>
  </Grid>
);

export default Friend;
