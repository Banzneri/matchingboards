import { useState } from 'react';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import TimePicker from '@mui/lab/TimePicker';
import { Box, TextField } from '@mui/material';
import theme from '../assets/Themes';

export default function SessionTimePicker({ setSelectedTime }) {
  const [value, setValue] = useState({});

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Box style={theme.centeredBox}>
        <TimePicker
          label="Pick a time, any time"
          value={value}
          onChange={(newValue) => {
            const checkHours =
              (newValue.getHours() < 10 ? '0' : '') + newValue.getHours();
            const checkMinutes =
              (newValue.getMinutes() < 10 ? '0' : '') + newValue.getMinutes();
            const selectedTime = `${checkHours}:${checkMinutes}`;
            setValue(newValue);
            setSelectedTime(selectedTime);
          }}
          renderInput={(params) => (
            <TextField
              style={theme.dateTimeInput}
              variant="filled"
              {...params}
            />
          )}
        />
      </Box>
    </LocalizationProvider>
  );
}
