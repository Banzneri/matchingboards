import { useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import { Snackbar } from '@mui/material';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import axios from 'axios';
import { useUser } from '../../contexts/UserContext';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: 'rgba(228, 212, 179, 0.8)',
  '&:hover': {
    backgroundColor: 'rgba(228, 212, 179, 0.9)',
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '22ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

export default function SearchBar({
  input,
  onChange,
  placeholder,
  givenURL,
  setUsernameList,
}) {
  const [error, setError] = useState('');
  const [open, setOpen] = useState(false);

  const { token } = useUser();

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    const trimmedInput = input.trim();
    if (trimmedInput.length === 0) {
      setUsernameList([]);
    } else {
      axios
        .get(`http://localhost:3001/${givenURL}${trimmedInput}`, {
          headers: { Authorization: token },
        })
        .then((response) => {
          setUsernameList(response.data);
        })
        .catch((err) => {
          if (err) setError(err.response.data.message);
          setOpen(true);
        });
    }
  }, [input]);
  return (
    <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
      noValidate
      autoComplete="off"
    >
      <Search>
        <SearchIconWrapper>
          <SearchIcon color="secondary" />
        </SearchIconWrapper>
        <StyledInputBase
          input={input}
          type="text"
          onChange={onChange}
          placeholder={placeholder}
        />
      </Search>
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        message={error}
      />
    </Box>
  );
}
