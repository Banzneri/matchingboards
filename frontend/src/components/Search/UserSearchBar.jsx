import { useState, useEffect } from 'react';
import SearchBar from './SearchBar';
import SearchListItem from './SearchListItem';

function UsernameList({ usernameList = [] }) {
  return (
    <>
      {usernameList.map((data) => {
        if (data) {
          const key = data.userName;
          const listResult = data.userName;
          const { id } = data;
          return (
            <SearchListItem
              key={key}
              listResult={listResult}
              to={{
                pathname: `/profile/${id}`,
                state: null,
              }}
            />
          );
        }
        return null;
      })}
    </>
  );
}

export default function UserSearchBar({ setHide }) {
  const [usernameList, setUsernameList] = useState();
  const [input, setInput] = useState('');
  const givenURL = 'users/search/';

  useEffect(() => {
    if (usernameList?.length > 0) {
      setHide(true);
    } else setHide(false);
  }, [usernameList]);
  return (
    <>
      <SearchBar
        onChange={(e) => setInput(e.target.value)}
        placeholder="Search by username"
        input={input}
        givenURL={givenURL}
        setUsernameList={setUsernameList}
      />
      <UsernameList usernameList={usernameList} />
    </>
  );
}
