import { useState, useEffect } from 'react';
import { debounce } from 'lodash';
import SearchBar from './SearchBar';
import SearchListItem from './SearchListItem';

const GameResult = ({ gameList = [] }) => (
  <>
    {gameList.map((game) => {
      if (game) {
        const key = game.id;
        const listResult = game.name;
        return (
          <SearchListItem
            key={key}
            listResult={listResult}
            to={{
              pathname: '/game',
              state: game,
            }}
          />
        );
      }
      return null;
    })}
  </>
);

const GameSearchBar = ({ setHide }) => {
  const [usernameList, setUsernameList] = useState();
  const [input, setInput] = useState('');
  const givenURL = 'games/search/name?queryString=';

  useEffect(() => {
    if (usernameList?.length > 0) {
      setHide(true);
    } else setHide(false);
  }, [usernameList]);

  return (
    <>
      <SearchBar
        onChange={debounce((e) => setInput(e.target.value), 500)}
        placeholder="Search a game"
        input={input}
        setUsernameList={setUsernameList}
        givenURL={givenURL}
      />
      <GameResult gameList={usernameList} />
    </>
  );
};

export default GameSearchBar;
