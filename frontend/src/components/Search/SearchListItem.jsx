import { Box, List, ListItem, ListItemText } from '@mui/material';
import { Link } from 'react-router-dom';
import theme from '../../assets/Themes';

export default function SearchListItem({ listResult, to }) {
  return (
    <Box
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <List style={theme.listItem}>
        <ListItem button component={Link} to={to.pathname} state={to.state}>
          <ListItemText primary={listResult} />
        </ListItem>
      </List>
    </Box>
  );
}
