import { Box } from '@mui/material';
import { altStyle } from '../../styles';

function DefaultGameImage() {
  return (
    <Box
      sx={{
        height: '15rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background: altStyle.backgroundColor,
        border: '1px black solid',
      }}
    >
      <i
        className="fa-solid fa-xl fa-dice"
        style={{
          color: 'rgba(228, 212, 179)',
        }}
      />
    </Box>
  );
}

export default DefaultGameImage;
