import { Checkbox, FormControlLabel } from '@mui/material';

function ToggleOwnedCheckbox({ handleToggleOwned, onList, owned }) {
  if (!onList) return null;

  return (
    <FormControlLabel
      control={
        <Checkbox
          sx={{
            '&.Mui-checked': {
              color: 'green',
            },
          }}
          checked={owned}
          onChange={handleToggleOwned}
        />
      }
      label="Owned"
    />
  );
}

export default ToggleOwnedCheckbox;
