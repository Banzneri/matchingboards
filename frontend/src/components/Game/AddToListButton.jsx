import { Button } from '@mui/material';
import React from 'react';

function RemoveButton({ handleRemove }) {
  return (
    <Button
      onClick={handleRemove}
      style={{
        width: '100%',
        margin: 0,
        borderRadius: 0,
      }}
      color="error"
      variant="contained"
    >
      Remove from list
    </Button>
  );
}

function AddButton({ handleAdd }) {
  return (
    <Button
      onClick={handleAdd}
      style={{
        width: '100%',
        margin: 0,
        borderRadius: 0,
      }}
      color="success"
      variant="contained"
    >
      Add to list
    </Button>
  );
}

function AddToListButton({ handleAdd, handleRemove, onList }) {
  return onList ? (
    <RemoveButton handleRemove={handleRemove} />
  ) : (
    <AddButton handleAdd={handleAdd} />
  );
}

export default AddToListButton;
