import {
  AppBar,
  Badge,
  Box,
  IconButton,
  Menu,
  MenuItem,
  Snackbar,
  Toolbar,
  Typography,
} from '@mui/material';
import { useState, useEffect } from 'react';
import axios from 'axios';
import AccountCircle from '@mui/icons-material/AccountCircle';
import NotificationsIcon from '@mui/icons-material/Notifications';
import MoreIcon from '@mui/icons-material/MoreVert';
import SettingsIcon from '@mui/icons-material/Settings';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import HomeIcon from '@mui/icons-material/Home';
import { Link, useNavigate } from 'react-router-dom';
import Logout from './Logout';
import DeleteAccount from './DeleteAccount';
import { useUser } from '../contexts/UserContext';
import logo from '../assets/logoMatchingBoards.png';

export default function NavBar() {
  const [open, setOpen] = useState(false);
  const [error, setError] = useState();
  const navigate = useNavigate();
  const { currentUser, loggedIn, token } = useUser();
  const [notificationsCount, setNotificationsCount] = useState([]);

  const updateNotifications = () => {
    if (token !== null) {
      axios
        .get(`http://localhost:3001/notifications/`, {
          headers: { Authorization: token },
        })
        .then((response) => {
          setNotificationsCount(response.data.friendRequests.length);
          // TODO: Add other notifications to the count when they are implemented
        })
        .catch(() => {
          setError(`Error finding your notifications.`);
          setOpen(true);
        });
    }
  };

  useEffect(() => {
    updateNotifications();
    const interval = setInterval(() => {
      updateNotifications();
    }, 5000);

    return () => {
      clearInterval(interval);
    };
  }, [token]);

  const handleClose = () => {
    setOpen(false);
  };

  const [anchorEl, setAnchorEl] = useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleSettingsMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
      onClick={handleMenuClose}
    >
      <MenuItem
        onClick={() => {
          navigate('/changepassword');
        }}
        style={{ backgroundColor: 'rgba(52, 45, 43, .959)' }}
      >
        <Typography variant="white">Change password</Typography>
      </MenuItem>
      <MenuItem style={{ backgroundColor: 'rgba(52, 45, 43, 0.959)' }}>
        <DeleteAccount />
      </MenuItem>
      <MenuItem style={{ backgroundColor: 'rgba(52, 45, 43, 0.959)' }}>
        <Logout />
      </MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem
        onClick={() => {
          navigate('/notifications');
        }}
        style={{ backgroundColor: 'rgba(52, 45, 43, 0.959)' }}
      >
        <IconButton
          size="large"
          aria-label="show new notifications"
          color="beige"
        >
          {notificationsCount > 0 ? (
            <Badge badgeContent={notificationsCount} color="error">
              <NotificationsIcon />
            </Badge>
          ) : (
            <NotificationsIcon />
          )}
        </IconButton>
        <Typography variant="white">Notifications</Typography>
      </MenuItem>
      <MenuItem
        onClick={handleSettingsMenuOpen}
        style={{ backgroundColor: 'rgba(52, 45, 43, 0.959)' }}
      >
        <IconButton
          size="large"
          aria-label="settings"
          aria-controls="primary-settings-menu"
          aria-haspopup="true"
          color="beige"
        >
          <SettingsIcon />
        </IconButton>
        <Typography variant="white">Settings</Typography>
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static" color="secondary">
          <Toolbar>
            <Link to="/">
              <img
                style={{
                  height: '4rem',
                  marginTop: '0.5rem',
                  marginBottom: '0.3rem',
                }}
                src={logo}
                alt="logo"
              />
            </Link>
            <Box sx={{ flexGrow: 1 }} />
            {loggedIn ? (
              <>
                <IconButton
                  size="large"
                  aria-label="home"
                  color="beige"
                  onClick={() => {
                    navigate('/');
                  }}
                >
                  <HomeIcon />
                </IconButton>
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  color="beige"
                  onClick={() => {
                    navigate(`/profile/${currentUser.id}`);
                  }}
                >
                  <AccountCircle />
                </IconButton>
                <IconButton
                  size="large"
                  aria-label="friends of current user"
                  color="beige"
                  onClick={() => {
                    navigate('/friendgroups');
                  }}
                >
                  <PeopleAltIcon />
                </IconButton>
              </>
            ) : null}
            <Box sx={{ display: { xs: 'none', sm: 'flex' } }}>
              {loggedIn ? (
                <>
                  <IconButton
                    size="large"
                    aria-label="show new notifications"
                    color="beige"
                    onClick={() => {
                      navigate('/notifications');
                    }}
                  >
                    {notificationsCount > 0 ? (
                      <Badge badgeContent={notificationsCount} color="error">
                        <NotificationsIcon />
                      </Badge>
                    ) : (
                      <NotificationsIcon />
                    )}
                  </IconButton>
                  <IconButton
                    size="large"
                    edge="end"
                    aria-label="settings"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleSettingsMenuOpen}
                    color="beige"
                  >
                    <SettingsIcon />
                  </IconButton>
                </>
              ) : null}
            </Box>
            <Box sx={{ display: { xs: 'flex', sm: 'none' } }}>
              {loggedIn ? (
                <IconButton
                  size="large"
                  aria-label="show more"
                  aria-controls={mobileMenuId}
                  aria-haspopup="true"
                  onClick={handleMobileMenuOpen}
                  color="beige"
                >
                  <MoreIcon />
                </IconButton>
              ) : null}
            </Box>
          </Toolbar>
        </AppBar>
        {renderMobileMenu}
        {renderMenu}
      </Box>
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        message={error}
      />
    </>
  );
}
