import React from 'react';
import { Box, Typography, Paper, Avatar } from '@mui/material';
import { Link } from 'react-router-dom';

function GameCard({ game }) {
  const paperStyle = {
    padding: 5,
    height: 'auto',
    borderRadius: '5px',
    width: 'auto',
    maxWidth: '300px',
    minWidth: '280px',
    margin: 'auto',
    backgroundColor: 'rgba(52, 45, 43, 0.959)',
    color: 'white',
    cursor: 'pointer',
  };

  return (
    <Paper style={paperStyle}>
      <Box
        sx={{ display: 'flex', textDecoration: 'none', color: 'white' }}
        component={Link}
        to="/game"
        state={game}
      >
        <Box>
          <Avatar
            sx={{
              backgroundColor: 'crimson',
              margin: '10px',
              width: '120px',
              height: 'auto',
            }}
            src={game.imageUrl}
            variant="square"
          />
        </Box>
        {/* TODO: Fix this eventually so the overflow: 'hidden' is not needed.
            18 characters go over the card so make the limit 17 characters. */}
        <Box sx={{ overflow: 'hidden' }}>
          <Typography variant="h6" sx={{ display: 'flex', margin: '10px' }}>
            {game.name}
          </Typography>
          <hr />
          <Typography sx={{ margin: '5px' }}>{game.releaseYear}</Typography>
        </Box>
      </Box>
    </Paper>
  );
}

export default GameCard;
