import { Box } from '@mui/material';

function ProfileTabPanel({ children, tabValue, index }) {
  return (
    <div role="tabpanel" hidden={tabValue !== index}>
      {tabValue === index && <Box>{children}</Box>}
    </div>
  );
}

export default ProfileTabPanel;
