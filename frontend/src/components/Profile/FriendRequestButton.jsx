import { Button } from '@mui/material';
import React from 'react';

function NonActiveButton({ text }) {
  return (
    <Button color="secondary" sx={{ margin: '1.5rem' }} variant="contained">
      {text}
    </Button>
  );
}

function ActiveButton({ handleRequest }) {
  return (
    <Button
      onClick={handleRequest}
      color="success"
      sx={{ margin: '1.5rem' }}
      variant="contained"
    >
      Send friend request
    </Button>
  );
}

function FriendRequestButton({ handleRequest, friendStatus }) {
  switch (friendStatus) {
    case 'pending':
      return <NonActiveButton text="Friend request sent" />;
    case 'rejected':
      return null;
    case 'accepted':
      return <NonActiveButton text="Friends" />;
    default:
      return <ActiveButton handleRequest={handleRequest} />;
  }
}

export default FriendRequestButton;
