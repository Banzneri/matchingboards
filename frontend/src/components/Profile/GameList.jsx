import { Grid } from '@mui/material';
import React from 'react';
import GameCard from '../GameCard';

const GameList = ({ games }) => (
  <Grid container spacing={4} sx={{ justifyContent: 'center', padding: '0' }}>
    {games.length > 0
      ? games.map((game) => (
          <Grid item key={game.id} md={6} sx={{ textAlign: 'center' }}>
            <GameCard game={game} />
          </Grid>
        ))
      : null}
  </Grid>
);

export default GameList;
