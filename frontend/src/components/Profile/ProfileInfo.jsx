import {
  Container,
  Grid,
  Snackbar,
  Tab,
  Tabs,
  Typography as Text,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { altStyle } from '../../styles';
import PersonalInfo from './PersonalInfo';
import ProfilePicture from './ProfilePicture';
import FriendRequestButton from './FriendRequestButton';
import EditProfileButton from './EditProfileButton';
import PublicInfo from './PublicInfo';
import { useUser } from '../../contexts/UserContext';
import ProfileTabPanel from './ProfileTabPanel';
import OwnSessions from '../Session/OwnSessions';
import GameLists from './GameLists';

function ProfileInfo({ id }) {
  const [profileInfo, setProfileInfo] = useState({
    ownedGames: [],
    wishedGames: [],
    user: {},
    friendStatus: '',
  });
  const [message, setMessage] = useState('');
  const [open, setOpen] = useState(false);
  const [tabValue, setTabValue] = useState(0);

  const { currentUser, token } = useUser();

  const isOwnProfile = currentUser.id === Number(id);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const ownedGames = await axios.get(
          `http://localhost:3001/games/users/${id}?owned=true`,
        );
        const wishedGames = await axios.get(
          `http://localhost:3001/games/users/${id}?owned=false`,
        );
        const user = isOwnProfile
          ? currentUser
          : await axios.get(`http://localhost:3001/users/${id}`);
        const friendStatus = isOwnProfile
          ? ''
          : await axios.get(
              `http://localhost:3001/friends/checkFriendStatus?friendId=${Number(
                id,
              )}&userId=${currentUser.id}`,
              {
                headers: { Authorization: token },
              },
            );

        setProfileInfo({
          ownedGames: ownedGames.data,
          wishedGames: wishedGames.data,
          user: isOwnProfile ? user : user.data,
          friendStatus: friendStatus.data,
        });
      } catch (err) {
        setOpen(true);
        setMessage(err.response.data);
      }
    };

    fetchData();
  }, [id]);

  const handleClose = () => {
    setOpen(false);
  };

  const handleFriendRequest = () => {
    axios
      .post(
        `http://localhost:3001/friends/add/${id}`,
        {},
        {
          headers: {
            Authorization: token,
          },
        },
      )
      .then(() => {
        setOpen(true);
        setMessage('Friend request sent.');
        setProfileInfo({
          user: profileInfo.user,
          ownedGames: profileInfo.ownedGames,
          wishedGames: profileInfo.wishedGames,
          friendStatus: 'pending',
        });
      })
      .catch((err) => {
        setOpen(true);
        setMessage(err.response.data.message);
      });
  };

  return (
    <Container style={altStyle}>
      <Text sx={{ display: 'inline' }} variant="h2">
        Character sheet
      </Text>
      <Grid container spacing={4}>
        <Grid item md={4} xs={12}>
          <br />
          <ProfilePicture />
          <br />
          <PublicInfo user={profileInfo.user} />
          <br />
          <PersonalInfo
            user={profileInfo.user}
            isOwnProfile={isOwnProfile}
            numberOfGames={profileInfo.ownedGames.length}
          />
          {isOwnProfile ? (
            <EditProfileButton />
          ) : (
            <FriendRequestButton
              handleRequest={handleFriendRequest}
              friendStatus={profileInfo.friendStatus}
            />
          )}
        </Grid>
        <Grid
          item
          md={8}
          xs={12}
          sx={{ textAlign: 'center', justifyContent: 'center' }}
        >
          <Tabs
            value={tabValue}
            onChange={(event, value) => setTabValue(value)}
            variant="fullWidth"
            sx={{
              borderBottom: 1,
              borderColor: 'divider',
            }}
            textColor="inherit"
          >
            <Tab label="games" />
            <Tab label="sessions" />
          </Tabs>
          <br />
          <br />
          <ProfileTabPanel tabValue={tabValue} index={0}>
            <GameLists isOwnProfile={isOwnProfile} profileInfo={profileInfo} />
          </ProfileTabPanel>
          <ProfileTabPanel tabValue={tabValue} index={1}>
            <OwnSessions user={profileInfo.user} />
          </ProfileTabPanel>
        </Grid>
      </Grid>
      <Snackbar
        open={open}
        autoHideDuration={2500}
        onClose={handleClose}
        message={message}
      />
      <br />
    </Container>
  );
}

export default ProfileInfo;
