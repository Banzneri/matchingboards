import { Divider, Typography as Text } from '@mui/material';
import { useState } from 'react';
import GameSearchBar from '../Search/GameSearchBar';
import GameList from './GameList';

const GameLists = ({ isOwnProfile, profileInfo }) => {
  const [hide, setHide] = useState(false);
  return (
    <>
      {isOwnProfile ? <GameSearchBar setHide={setHide} /> : null}
      <br />
      {!hide ? (
        <>
          <Text sx={{ textAlign: 'center' }} variant="h4">
            Owned games
          </Text>
          <Divider sx={{ background: 'grey', marginTop: '0.3rem' }} />
          <br />
          <GameList games={profileInfo.ownedGames} />
          <br />
          <Text sx={{ textAlign: 'center' }} variant="h4">
            Games I wish to play
          </Text>
          <Divider sx={{ background: 'grey', marginTop: '0.3rem' }} />
          <br />
          <GameList games={profileInfo.wishedGames} />
        </>
      ) : null}
    </>
  );
};

export default GameLists;
