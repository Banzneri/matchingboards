import { TextField } from '@mui/material';

function MultiLineInputForCreateSessionForm({
  label,
  value,
  onChange,
  ...props
}) {
  return (
    <TextField
      variant="filled"
      className="text-input"
      label={props.id}
      value={value}
      onChange={onChange}
      style={{
        backgroundColor: 'rgba(228, 212, 179, 0.824)',
        borderRadius: '5px',
        marginTop: '30px',
      }}
      {...props}
      fullWidth
      required
      multiline
      rows={3}
    />
  );
}

export default MultiLineInputForCreateSessionForm;
