export default function SelectInput({ label, value, onChange, ...props }) {
  return (
    <div>
      <label htmlFor={props.id || props.name}>{label}</label>
      <select value={value} required onChange={onChange} {...props} />
    </div>
  );
}
