import {
  Box,
  Container,
  Grid,
  Snackbar,
  Stack,
  Button,
  Typography as Text,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import { altStyle } from '../styles';
import { useUser } from '../contexts/UserContext';
import AddToListButton from '../components/Game/AddToListButton';
import ToggleOwnedCheckbox from '../components/Game/ToggleOwnedCheckbox';
import DefaultGameImage from '../components/Game/DefaultGameImage';

function Game() {
  const [ownedStatus, setOwnedStatus] = useState({
    owned: false,
    onList: false,
  });
  const [message, setMessage] = useState('');
  const [open, setOpen] = useState(false);
  const [showFullText, setShowFullText] = useState(false);

  const location = useLocation();
  const { token } = useUser();

  const game = location.state;

  useEffect(() => {
    const update = async () => {
      try {
        const usersGame = await axios.get(
          `http://localhost:3001/games/users/game/${game.id}`,
          {
            headers: {
              Authorization: token,
            },
          },
        );

        setOwnedStatus({
          owned: usersGame.data.owned,
          onList: !!usersGame.data,
        });
      } catch (error) {
        setMessage(error.response.data);
        setOpen(true);
      }
    };
    update();
  }, []);

  const handleAdd = async () => {
    try {
      await axios.post(
        `http://localhost:3001/games/users/${game.id}?owned=false`,
        {},
        { headers: { Authorization: token } },
      );
      setOwnedStatus({ owned: ownedStatus.owned, onList: true });
    } catch (error) {
      setMessage(error.response.data);
      setOpen(true);
    }
  };

  const handleRemove = async () => {
    try {
      await axios.delete(`http://localhost:3001/games/users/${game.id}`, {
        headers: { Authorization: token },
      });
      setOwnedStatus({ owned: false, onList: false });
    } catch (error) {
      setMessage(error.response.data);
      setOpen(true);
    }
  };

  const handleToggleOwned = async (event, isOwned) => {
    try {
      await axios.put(
        `http://localhost:3001/games/users/${game.id}/?owned=${isOwned}`,
        {},
        { headers: { Authorization: token } },
      );
      setOwnedStatus({ owned: isOwned, onList: ownedStatus.onList });
    } catch (error) {
      setMessage(error.response.data);
      setOpen(true);
    }
  };

  return (
    <Container style={altStyle}>
      <Box sx={{ padding: '2rem' }}>
        <Text variant="h2">{game.name}</Text>
      </Box>
      <Grid container justifyItems="center" spacing={5}>
        <Grid item md={6}>
          {game.imageUrl ? (
            <img
              src={game.imageUrl}
              alt="kuva"
              style={{
                width: '100%',
              }}
            />
          ) : (
            <DefaultGameImage />
          )}
          <Stack direction="row" spacing={1}>
            <AddToListButton
              handleAdd={handleAdd}
              handleRemove={handleRemove}
              onList={ownedStatus.onList}
            />
            <ToggleOwnedCheckbox
              handleToggleOwned={handleToggleOwned}
              onList={ownedStatus.onList}
              owned={ownedStatus.owned}
            />
          </Stack>
        </Grid>
        <Grid item md={6} sx={{ height: 'auto' }}>
          <Text
            sx={{
              height: showFullText ? '97%' : '360px',
              overflowY: 'hidden',
            }}
          >
            {game.description}
          </Text>
          {game.description.length > 1250 && (
            <Button
              variant="contained"
              onClick={() => setShowFullText(!showFullText)}
              sx={{ width: '100%' }}
            >
              {showFullText ? 'Collapse' : 'Show full description'}
            </Button>
          )}
        </Grid>
      </Grid>
      <Snackbar
        open={open}
        autoHideDuration={2500}
        onClose={() => setOpen(false)}
        message={message}
      />
    </Container>
  );
}

export default Game;
