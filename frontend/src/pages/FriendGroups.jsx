import React, { useState, useEffect } from 'react';
import {
  Box,
  Paper,
  Typography,
  Grid,
  Snackbar,
  IconButton,
} from '@mui/material';
import { Close as CloseIcon } from '@mui/icons-material';
import axios from 'axios';
import { useUser } from '../contexts/UserContext';
import UserSearchBar from '../components/Search/UserSearchBar';
import Friend from '../components/Friend';

const FriendGroups = () => {
  const [friends, setFriends] = useState([]);
  const [openError, setOpenError] = useState(false);
  const [error, setError] = useState(null);
  const [hide, setHide] = useState(false);
  const { currentUser, token } = useUser();
  const { id } = currentUser;

  const paperStyle = {
    padding: 30,
    height: 'auto',
    borderRadius: '15px',
    width: '75vw',
    margin: '20px auto',
    backgroundColor: 'rgba(52, 45, 43, 0.959)',
    color: 'white',
  };

  const handleClose = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenError(false);
  };

  const action = (
    <IconButton
      size="small"
      aria-label="close"
      color="inherit"
      onClick={handleClose}
    >
      <CloseIcon fontSize="small" />
    </IconButton>
  );

  useEffect(() => {
    axios
      .get(`http://localhost:3001/friends/id/${id}`, {
        headers: { Authorization: token },
      })
      .then((response) => {
        setFriends(response.data);
      })
      .catch((err) => {
        if (err && err.response) setError('Could not fetch friends.');
        setOpenError(true);
      });
  }, []);

  const mappedFriendData = friends?.map((friend) => (
    <Friend key={friend.id} friend={friend} />
  ));

  return (
    <Box>
      <Paper style={paperStyle}>
        <Typography variant="h3" style={{ textAlign: 'center' }}>
          Friends
        </Typography>
        <hr />
        <Grid
          item
          md="auto"
          xs="auto"
          style={{ textAlign: 'center', justifyContent: 'center' }}
        >
          <br />
          <Grid container spacing={2} style={{ justifyContent: 'center' }}>
            <Grid
              item
              xs={10}
              md={4}
              style={{
                textAlign: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography>Search friends</Typography>
              <UserSearchBar setHide={setHide} />
            </Grid>
          </Grid>
          {!hide ? (
            <Box style={{ margin: '15px', padding: '10px' }}>
              <Grid container style={{ justifyContent: 'center' }}>
                {mappedFriendData}
              </Grid>
            </Box>
          ) : null}
        </Grid>
      </Paper>
      <Snackbar
        open={openError}
        autoHideDuration={6000}
        onClose={handleClose}
        message={error}
        action={action}
      />
    </Box>
  );
};

export default FriendGroups;
