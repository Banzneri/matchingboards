import { Container, Snackbar } from '@mui/material';
import { useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useUser } from '../contexts/UserContext';
import SessionInfoView from '../components/Session/SessionInfoView';

function Session() {
  const [errorMessage, setErrorMessage] = useState('');
  const [open, setOpen] = useState(false);

  const location = useLocation();
  const session = location.state;
  const { token, currentUser } = useUser();

  return (
    <Container>
      <SessionInfoView
        currentUser={currentUser}
        token={token}
        setErrorMessage={setErrorMessage}
        setOpen={setOpen}
        session={session}
      />
      <Snackbar
        open={open}
        autoHideDuration={2500}
        onClose={() => setOpen(false)}
        message={errorMessage}
      />
    </Container>
  );
}

export default Session;
