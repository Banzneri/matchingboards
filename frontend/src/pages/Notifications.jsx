import React, { useEffect, useState } from 'react';
import { Snackbar } from '@mui/material';
import axios from 'axios';
import FriendRequest from '../components/Notifications/FriendRequest';
import { useUser } from '../contexts/UserContext';

const Notifications = () => {
  const { token } = useUser();
  const [open, setOpen] = useState(false);
  const [error, setError] = useState();

  const [notifications, setNotifications] = useState([]);

  const updateNotifications = () => {
    axios
      .get(`http://localhost:3001/notifications/`, {
        headers: { Authorization: token },
      })
      .then((response) => {
        setNotifications(response.data);
      })
      .catch(() => {
        setError(`Error finding your notifications.`);
        setOpen(true);
      });
  };

  useEffect(() => {
    updateNotifications();
  }, []);

  const handleClose = () => {
    setOpen(false);
  };

  // TODO: implement other notifications as well (sessionRequests, messages)
  const friendData = notifications?.friendRequests;
  const mappedFriendData = friendData?.map((friendRequest) => (
    <FriendRequest
      senderId={friendRequest.senderId}
      key={friendRequest.senderId}
      updateNotifications={updateNotifications}
    />
  ));

  return (
    <>
      {mappedFriendData}
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        message={error}
      />
    </>
  );
};

export default Notifications;
