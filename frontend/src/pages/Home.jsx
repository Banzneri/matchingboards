import React from 'react';
import CreateSessionModal from '../components/Session/CreateSessionModal';
import SessionListContainer from '../components/Session/SessionListContainer';

function Home() {
  return (
    <>
      <CreateSessionModal />
      <SessionListContainer />
    </>
  );
}

export default Home;
