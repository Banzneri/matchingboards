import React from 'react';
import { useParams } from 'react-router-dom';
import ProfileInfo from '../components/Profile/ProfileInfo';

function Profile() {
  const { id } = useParams();

  return <ProfileInfo id={id} />;
}

export default Profile;
